package me.taucu.modispensermechanics.util;

import java.util.Collection;
import java.util.function.Function;

public class CollectionUtil {

    public static <I, R, M extends R, O extends Collection<R>> O map(Collection<I> input, O output, Function<I, M> mapper, Class<R> as) {
        for (I i : input) output.add(mapper.apply(i));
        return output;
    }

}
