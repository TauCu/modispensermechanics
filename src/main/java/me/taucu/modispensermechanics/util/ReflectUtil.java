package me.taucu.modispensermechanics.util;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

public class ReflectUtil {

    public static Field resolveDeclaredFieldByEquality(Class<?> targetClazz, Object target, Object ref) {
        try {
            for (Field f : targetClazz.getDeclaredFields()) {
                f.setAccessible(true);
                if (f.get(target) == ref) {
                    return f;
                }
            }
        } catch (ReflectiveOperationException e) {
            throw new RuntimeException(e);
        }
        return null;
    }

    public static Method getMethod(Class<?> clazz, String name, Class<?>... parameterTypes) {
        try {
            return clazz.getMethod(name, parameterTypes);
        } catch (NoSuchMethodException e) {
            return null;
        }
    }

}
