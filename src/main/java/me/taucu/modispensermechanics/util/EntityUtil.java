package me.taucu.modispensermechanics.util;

import net.minecraft.world.entity.Entity;
import net.minecraft.world.phys.AABB;
import net.minecraft.world.phys.Vec3;

import java.util.Comparator;

import static org.bukkit.util.NumberConversions.square;

public class EntityUtil {

    public static Comparator<Entity> nearestComparator(Vec3 to) {
        return nearestComparator(to.x, to.y, to.z);
    }

    /**
     * Sorts by bounding box the closest to the provided coordinates, descending order.
     * @return Nearest first comparator
     */
    public static Comparator<Entity> nearestComparator(double x, double y, double z) {
        return Comparator.comparingDouble(ent -> {
            AABB aabb = ent.getBoundingBox();
            return square(aabb.minX - x) + square(aabb.maxX - x)
                    + square(aabb.minY - y) + square(aabb.maxY - y)
                    + square(aabb.minZ - z) + square(aabb.maxZ - z);
        });
    }

}
