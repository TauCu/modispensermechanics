package me.taucu.modispensermechanics.util;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.world.entity.item.ItemEntity;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.block.entity.DispenserBlockEntity;
import org.bukkit.craftbukkit.block.CraftDispenser;
import org.bukkit.persistence.PersistentDataContainer;

public class DispenserUtil {

    public static ItemEntity dropItem(ServerLevel level, BlockPos pos, Direction dir, ItemStack stack) {
        return dropItem(level, pos, dir, 6, stack);
    }

    public static ItemEntity dropItem(ServerLevel level, BlockPos pos, Direction dir, int speed, ItemStack stack) {
        double x = pos.getX() + 0.5D + (0.7D * dir.getStepX());
        double y = pos.getY() + 0.5D + (0.7D * dir.getStepY());
        double z = pos.getZ() + 0.5D + (0.7D * dir.getStepZ());
        y -= dir.getAxis() == Direction.Axis.Y ? 0.125D : 0.15625D;

        ItemEntity item = new ItemEntity(level, x, y, z, stack);

        double offset = level.random.nextDouble() * 0.1D + 0.2D;
        item.setDeltaMovement(
                level.random.triangle(dir.getStepX() * offset, 0.0172275D * speed),
                level.random.triangle(0.2D, 0.0172275D * speed),
                level.random.triangle(dir.getStepZ() * offset, 0.0172275D * speed)
                );

        level.addFreshEntity(item);
        return item;
    }

    @SuppressWarnings("UnreachableCode")
    public static PersistentDataContainer getPDC(DispenserBlockEntity dispenser) {
        PersistentDataContainer pdc = dispenser.persistentDataContainer;
        // PDC can be null on spigot/bukkit if it's never been used
        if (pdc == null) {
            pdc = (new CraftDispenser(dispenser.getLevel().getWorld(), dispenser)).getPersistentDataContainer();
        }
        return pdc;
    }

}
