package me.taucu.modispensermechanics.util;

import net.minecraft.core.Holder;
import net.minecraft.core.component.DataComponents;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.util.Mth;
import net.minecraft.world.entity.EquipmentSlot;
import net.minecraft.world.entity.ai.attributes.Attribute;
import net.minecraft.world.entity.ai.attributes.AttributeModifier;
import net.minecraft.world.entity.ai.attributes.Attributes;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.component.ItemAttributeModifiers;
import net.minecraft.world.item.enchantment.EnchantmentHelper;
import net.minecraft.world.level.block.state.BlockState;

import java.util.ArrayList;
import java.util.List;

public class ItemStackUtils {

    public static boolean hurtAndBreak(int amount, ItemStack stack, ServerLevel level) {
        if (hurt(amount, stack, level)) {
            stack.shrink(1);
            stack.setDamageValue(0);
            return true;
        }
        return false;
    }

    // modeled off of ItemStack#hurtAndBreak
    public static boolean hurt(int amount, ItemStack stack, ServerLevel level) {
        if (stack.isDamageableItem()) {

            if (amount > 0) {
                amount = EnchantmentHelper.processDurabilityChange(level, stack, amount);

                if (amount <= 0) {
                    return false;
                }
            }

            int newDamage = stack.getDamageValue() + amount;
            stack.setDamageValue(newDamage);
            return newDamage >= stack.getMaxDamage();
        } else {
            return false;
        }
    }

    public static double calculateAttributeWithEnchants(ItemStack stack, EquipmentSlot slot, Holder<Attribute> attribute, double base) {
        List<AttributeModifier> modifiers = new ArrayList<>();
        stack.forEachModifier(slot, (attr, mod) -> {
            if (attr.value().equals(attribute.value())) {
                modifiers.add(mod);
            }
        });

        return calculateAttributeValue(modifiers, attribute, base);
    }

    public static double calculateAttribute(ItemStack stack, EquipmentSlot slot, Holder<Attribute> attribute, double base) {
        var itemModifiers = stack.getComponents().get(DataComponents.ATTRIBUTE_MODIFIERS);
        if (itemModifiers == null)
            return attribute.value().sanitizeValue(base);

        List<AttributeModifier> modifiers = new ArrayList<>();
        for (ItemAttributeModifiers.Entry entry : itemModifiers.modifiers()) {
            if (entry.attribute().value().equals(attribute.value()) && entry.slot().test(slot)) {
                modifiers.add(entry.modifier());
            }
        }

        return calculateAttributeValue(modifiers, attribute, base);
    }

    // modeled off of AttributeInstance#calculateValue
    public static double calculateAttributeValue(List<AttributeModifier> modifiers, Holder<Attribute> attribute, double base) {
        if (modifiers.isEmpty())
            return attribute.value().sanitizeValue(base);

        for (var mod : modifiers) {
            if (mod.operation() == AttributeModifier.Operation.ADD_VALUE) {
                base += mod.amount();
            }
        }

        double value = base;

        for (var mod : modifiers) {
            if (mod.operation() == AttributeModifier.Operation.ADD_MULTIPLIED_BASE) {
                value += base * mod.amount();
            }
        }

        for (var mod : modifiers) {
            if (mod.operation() == AttributeModifier.Operation.ADD_MULTIPLIED_TOTAL) {
                value *= 1 + mod.amount();
            }
        }

        return attribute.value().sanitizeValue(value);
    }

    public static boolean couldToolBreak(ItemStack tool, BlockState state) {
        return couldToolBreak(tool, tool.isCorrectToolForDrops(state) ? 1 : 2);
    }

    public static boolean couldToolBreak(ItemStack tool, int damage) {
        return tool.isDamageableItem() && tool.getDamageValue() + damage >= tool.getMaxDamage();
    }

    // modeled off of Player#attack
    public static float getAttackDamageScale(long currentGameTime, long lastUsedGameTime, ItemStack stack, float baseTime) {
        double attackSpeed = calculateAttribute(stack, EquipmentSlot.MAINHAND, Attributes.ATTACK_SPEED, Attributes.ATTACK_SPEED.value().getDefaultValue());
        float attackStrengthDelay = (float) (1.0D / attackSpeed * 20.0D);
        return Mth.clamp((Math.max(0, currentGameTime - lastUsedGameTime) + baseTime) / attackStrengthDelay, 0.0F, 1.0F);
    }

}
