package me.taucu.modispensermechanics.util;

import me.taucu.modispensermechanics.MoDispenserMechanics;
import org.bukkit.Bukkit;
import org.bukkit.event.*;
import org.bukkit.plugin.RegisteredListener;
import org.bukkit.plugin.java.JavaPlugin;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.function.Consumer;

public class EventUtil {

    public static void enforcePriority(Listener listener, JavaPlugin plugin) {
        List<Class<? extends Event>> events = new ArrayList<>();
        Class<?> clazz = listener.getClass();
        do {
            for (Method m : clazz.getDeclaredMethods()) {
                if (m.getAnnotation(EventHandler.class) != null && m.getParameterCount() == 1 && Arrays.stream(m.getParameters()).anyMatch(p -> Event.class.isAssignableFrom(p.getType()))) {
                    enforcePriority(listener, m.getParameters()[0].getType(), plugin);
                }
            }
        } while ((clazz = clazz.getSuperclass()) != null && Listener.class.isAssignableFrom(clazz));
    }

    public static void enforcePriority(Listener listener, Class<?> eventClazz, JavaPlugin plugin) {
        HandlerList lst = getHandlerList(eventClazz);
        RegisteredListener[] listeners = lst.getRegisteredListeners();

        for (RegisteredListener rl : listeners) {
            lst.unregister(rl);
        }

        try {
            Arrays.sort(listeners, Comparator.comparingInt(a ->
                    a.getPriority().getSlot() + (a.getListener() == listener ? (a.getPriority().getSlot() > 0 ? 1 : -1) : 0)));
            for (RegisteredListener rl : listeners) {
                lst.register(rl);
            }
            lst.bake();
        } catch (Throwable t) {
            for (RegisteredListener rl : listeners) {
                lst.unregister(rl);
                lst.register(rl);
            }
            throw t;
        }
    }

    @SuppressWarnings("unchecked")
    public static <T extends Event> void registerHandler(Class<T> eventClazz, EventPriority priority, boolean ignoreCancelled, Consumer<T> handler, Listener dummy) {
        // if the supplied event class doesn't have a handler list, it'll check its superclasses until it finds one.
        // once found, it'll register the handler to the class it found the handler list in.
        // this means the handler can be called with superclasses of eventClazz, thus we must check if the objects emitted can be cast to eventClazz.
        if (checkForHandlerList(eventClazz)) {
            // eventClazz has handler list; all events emitted will be either of eventClazz or children of it.
            Bukkit.getPluginManager().registerEvent(eventClazz, dummy, priority, (l, e) -> {
                handler.accept((T) e);
            }, MoDispenserMechanics.getInstance(), ignoreCancelled);
        } else {
            // eventClazz doesn't have a handler list; must check if event instance can be cast to eventClazz.
            Bukkit.getPluginManager().registerEvent(eventClazz, dummy, priority, (l, e) -> {
                if (eventClazz.isInstance(e)) {
                    handler.accept((T) e);
                }
            }, MoDispenserMechanics.getInstance(), ignoreCancelled);
        }
    }

    public static boolean checkForHandlerList(Class<?> clazz) {
        try {
            clazz.getDeclaredMethod("getHandlerList");
            return true;
        } catch (NoSuchMethodException e) {
            return false;
        }
    }

    public static HandlerList getHandlerList(Class<?> eventClazz) {
        try {
            return (HandlerList) eventClazz.getMethod("getHandlerList").invoke(null);
        } catch (ReflectiveOperationException e) {
            throw new RuntimeException(e);
        }
    }

}
