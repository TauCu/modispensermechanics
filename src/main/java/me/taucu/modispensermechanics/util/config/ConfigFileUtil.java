package me.taucu.modispensermechanics.util.config;

import com.google.common.io.ByteStreams;
import org.bukkit.configuration.Configuration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.*;
import java.nio.file.Files;

public class ConfigFileUtil {

    public static boolean minorMismatch(Configuration conf, Configuration defaults) {
        return conf.getDouble("version", -1) != defaults.getDouble("version", -2);
    }

    public static boolean majorMismatch(Configuration conf, Configuration defaults) {
        return ((int) conf.getDouble("version", -1)) != ((int) defaults.getDouble("version", -2));
    }

    public static File renameOldConfig(File oldConfig) {
        try {
            String oldExtension = getExtension(oldConfig);
            for (int i = 0; i < 100; i++) {
                File dest = changeExtension(oldConfig, ".old." + i + oldExtension);
                if (!dest.isFile()) {
                    return Files.move(oldConfig.toPath(), dest.toPath()).toFile();
                }
            }
            throw new RuntimeException("Couldn't rename config (too many old configs)");
        } catch (IOException e) {
            throw new RuntimeException("IOException while renaming old config", e);
        }
    }

    public static String getExtension(File f) {
        String name = f.getName();
        return name.substring(name.lastIndexOf('.'));
    }

    public static File changeExtension(File f, String newExtension) {
        String name = f.getName();
        String strippedName = name.substring(0, name.lastIndexOf('.'));
        return new File(f.getParentFile(), strippedName + newExtension);
    }

    public static InputStream streamInternalConfig() {
        return ConfigFileUtil.class.getClassLoader().getResourceAsStream("config.yml");
    }

    public static YamlConfiguration loadInternalConfig() {
        try (InputStreamReader in = new InputStreamReader(streamInternalConfig())) {
            return YamlConfiguration.loadConfiguration(in);
        } catch (IOException e) {
            throw new RuntimeException("could not load internal config", e);
        }
    }

    public static void copyInternalFile(File dest, String internalFile) throws IOException {
        File parent = dest.getParentFile();
        if (parent != null) {
            parent.mkdirs();
        }
        if (!dest.createNewFile()) {
            throw new IOException("Could not create file!");
        }
        try (InputStream is = ConfigFileUtil.class.getClassLoader().getResourceAsStream(internalFile); OutputStream os = new FileOutputStream(dest)) {
            ByteStreams.copy(is, os);
        }
    }

}
