package me.taucu.modispensermechanics.util;

import net.minecraft.server.MinecraftServer;
import org.bukkit.Bukkit;
import org.bukkit.craftbukkit.CraftServer;

import java.lang.StackWalker.StackFrame;
import java.util.List;
import java.util.stream.Stream;

public class BukkitUtil {

    public static final boolean IS_PAPER = isPaper();
    public static final boolean IS_FOLIA = isFolia();

    public static boolean isRunning() {
        return MinecraftServer.getServer().getTickCount() > 0;
    }

    public static boolean isShuttingDown() {
        List<StackFrame> frames = StackWalker.getInstance(StackWalker.Option.RETAIN_CLASS_REFERENCE)
                .walk(Stream::toList);

        for (int i = 0; i < frames.size(); i++) {
            StackFrame frame = frames.get(i);
            if (CraftServer.class.isAssignableFrom(frame.getDeclaringClass())
                    && frame.getMethodName().equals("disablePlugins")
                    && frames.size() > i+2) {
                if (frames.get(i+1).getDeclaringClass().getPackageName().startsWith("net.minecraft.server")
                        && frames.get(i+1).getMethodType().parameterCount() == 0
                        && frames.get(i+1).getMethodType().returnType() == void.class
                        && frames.get(i+2).getDeclaringClass().getPackageName().startsWith("net.minecraft.server")) {
                    return true;
                }
            }
        }
        return false;
    }

    public static boolean isReloading() {
        return StackWalker.getInstance(StackWalker.Option.RETAIN_CLASS_REFERENCE)
                .walk(s -> s
                        .filter(f -> f.getMethodType().parameterCount() == 0)
                        .filter(f -> f.getMethodType().returnType() == void.class)
                        .filter(f -> CraftServer.class.isAssignableFrom(f.getDeclaringClass()))
                        .anyMatch(f -> f.getMethodName().equals("reload")));
    }

    public static String getMinecraftVersion() {
        return Bukkit.getBukkitVersion().split("-")[0];
    }

    private static boolean isPaper() {
        ClassLoader cl = Bukkit.class.getClassLoader();
        boolean bl;
        do {
            bl = cl.getResource("io/papermc/paper") != null || cl.getResource("io/papermc/paperclip") != null
                    || cl.getResource("com/destroystokyo/paper") != null || cl.getResource("com/destroystokyo/paperclip") != null;
        } while (!bl && (cl = cl.getParent()) != null);
        return bl;
    }

    private static boolean isFolia() {
        return classForName(Bukkit.getServer().getClass().getClassLoader(), "io.papermc.paper.threadedregions.RegionizedServer") != null;
    }

    private static Class<?> classForName(ClassLoader ldr, String name) {
        try {
            return ldr.loadClass(name);
        } catch (Exception e) {
            return null;
        }
    }

}
