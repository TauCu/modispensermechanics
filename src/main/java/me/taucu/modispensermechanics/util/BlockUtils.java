package me.taucu.modispensermechanics.util;

import net.minecraft.core.BlockPos;
import net.minecraft.world.entity.EquipmentSlot;
import net.minecraft.world.entity.ai.attributes.Attributes;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.state.BlockState;

public class BlockUtils {

    public static float getDestroyRate(Level level, BlockPos pos, BlockState state, ItemStack stack) {
        // get the destroySpeed of the tool used versus the block it is being used on
        // modeled off of Player#getDestroySpeed
        float destroySpeed = stack.getDestroySpeed(state);
        if (destroySpeed > 1.0F) {
            destroySpeed += (float) ItemStackUtils.calculateAttributeWithEnchants(stack, EquipmentSlot.MAINHAND, Attributes.MINING_EFFICIENCY, 0.0D);
        }
        // modeled off of BlockBehaviour#getDestroyProgress
        float hardness = state.getDestroySpeed(level, pos);
        if (hardness == -1.0F) { // unbreakable
            return 0.0F;
        } else if (hardness == 0) { // instamine
            return 1.0F;
        } else {
            return destroySpeed / hardness / (!state.requiresCorrectToolForDrops() || stack.isCorrectToolForDrops(state) ? 30 : 100);
        }
    }

}
