package me.taucu.modispensermechanics.listeners;

import com.destroystokyo.paper.event.entity.CreeperIgniteEvent;
import io.papermc.paper.event.block.PlayerShearBlockEvent;
import io.papermc.paper.event.player.PlayerFlowerPotManipulateEvent;
import io.papermc.paper.event.player.PlayerItemFrameChangeEvent;
import io.papermc.paper.event.player.PlayerNameEntityEvent;

public class PaperEntityListener extends EntityListener {

    public PaperEntityListener() {
        registerEventSuppressor(PlayerShearBlockEvent.class, this::handle);
        registerEventSuppressor(PlayerFlowerPotManipulateEvent.class, this::handle);
        registerEventSuppressor(PlayerNameEntityEvent.class, this::handle);
        registerEventSuppressor(CreeperIgniteEvent.class, this::handle);
        registerEventSuppressor(PlayerItemFrameChangeEvent.class, this::handle);
    }

}
