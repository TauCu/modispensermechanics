package me.taucu.modispensermechanics.listeners;

import me.taucu.modispensermechanics.MoDispenserMechanics;
import me.taucu.modispensermechanics.fakeplayer.FakePlayer;
import me.taucu.modispensermechanics.fakeplayer.FakePlayerManager;
import me.taucu.modispensermechanics.util.EventUtil;
import org.bukkit.craftbukkit.entity.CraftEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.*;
import org.bukkit.event.block.BellRingEvent;
import org.bukkit.event.block.BlockFertilizeEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.block.CauldronLevelChangeEvent;
import org.bukkit.event.entity.*;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.event.player.*;
import org.jetbrains.annotations.Nullable;

import java.util.HashSet;
import java.util.function.Predicate;

public class EntityListener implements Listener {

    public static boolean allowTaming;

    private final HashSet<Class<? extends Event>> suppressedEvents = new HashSet<>();

    public EntityListener() {
        registerEventSuppressor(EntityDamageByEntityEvent.class, e -> handle(e.getDamager()));
        registerEventSuppressor(PlayerBucketFillEvent.class, this::handle);
        registerEventSuppressor(PlayerBucketEmptyEvent.class, this::handle);
        registerEventSuppressor(CauldronLevelChangeEvent.class, e -> handle(e.getEntity()));
        registerEventSuppressor(BlockPlaceEvent.class, e -> handle(e.getPlayer()));
        registerEventSuppressor(EntityChangeBlockEvent.class, this::handle);
        registerEventSuppressor(BlockFertilizeEvent.class, e -> handle(e.getPlayer()));
        registerEventSuppressor(BellRingEvent.class, e -> handle(e.getEntity()));
        registerEventSuppressor(PlayerItemConsumeEvent.class, this::handle);
        registerEventSuppressor(PlayerGameModeChangeEvent.class, this::handle);
        registerEventSuppressor(PlayerLeashEntityEvent.class, e -> handle(e.getPlayer()));
        registerEventSuppressor(InventoryOpenEvent.class, e -> handle(e.getPlayer()));
        registerEventSuppressor(SheepDyeWoolEvent.class, e -> handle(e.getPlayer()));
        registerEventSuppressor(PlayerBucketEntityEvent.class, this::handle);
        registerEventSuppressor(PlayerShearEntityEvent.class, this::handle);
        registerEventSuppressor(PotionSplashEvent.class, e -> e.getPotion().getShooter() instanceof Player p && handle(p));
        registerEventSuppressor(EntityTargetEvent.class, e -> handle(e.getTarget()), e -> true);
        registerEventSuppressor(PlayerTeleportEvent.class, this::handle, e -> true);
        registerEventSuppressor(EntityTameEvent.class, e -> handle(e.getOwner()), e -> !allowTaming);
    }

    @SuppressWarnings("deprecation")
    @EventHandler(priority = EventPriority.MONITOR)
    public void onJoin(PlayerJoinEvent e) {
        // some plugins may have hidden the fake player
        e.getPlayer().showPlayer(FakePlayerManager.getFakePlayer().getBukkitEntity());
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onLogin(PlayerLoginEvent e) {
        MoDispenserMechanics.attachHandler(e.getPlayer(), e.getAddress());
    }

    public <T extends Event & Cancellable> void registerEventSuppressor(Class<T> eventClazz, Predicate<T> shouldSuppress) {
        registerEventSuppressor(eventClazz, shouldSuppress, null);
    }

    public <T extends Event & Cancellable> void registerEventSuppressor(Class<T> eventClazz, Predicate<T> shouldSuppress, @Nullable Predicate<T> shouldCancel) {
        if (suppressedEvents.contains(eventClazz)) return;
        EventUtil.registerHandler(eventClazz, EventPriority.LOWEST, false, e -> {
            if (shouldSuppress.test(e)) {
                e.setCancelled(true);
            }
        }, this);
        EventUtil.registerHandler(eventClazz, EventPriority.MONITOR, false, e -> {
            if (shouldSuppress.test(e)) {
                e.setCancelled(shouldCancel != null && shouldCancel.test(e));
            }
        }, this);
        suppressedEvents.add(eventClazz);
    }

    public void enforceListenerPriorities() {
        EventUtil.enforcePriority(this, MoDispenserMechanics.getInstance());
        for (var clazz : suppressedEvents) {
            EventUtil.enforcePriority(this, clazz, MoDispenserMechanics.getInstance());
        }
    }

    public boolean handle(PlayerEvent e) {
        return handle(e.getPlayer());
    }

    public boolean handle(EntityEvent e) {
        return handle(e.getEntity());
    }

    public boolean handle(Object ent) {
        return !FakePlayer.allowFakePlayerEvents && isFake(ent);
    }

    public static void handle(Cancellable c, Object ent, boolean state) {
        if (!FakePlayer.allowFakePlayerEvents && isFake(ent)) {
            c.setCancelled(state);
        }
    }

    public static boolean isFake(Object ent) {
        return ent instanceof CraftEntity ce && ce.getHandle() instanceof FakePlayer;
    }

}
