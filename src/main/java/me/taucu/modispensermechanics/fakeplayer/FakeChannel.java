package me.taucu.modispensermechanics.fakeplayer;

import io.netty.channel.*;
import me.taucu.modispensermechanics.util.SpaghetUtil;

import java.net.SocketAddress;

// packetevents checks for classes named "FakeChannel"
public class FakeChannel extends AbstractChannel {

    private static final ChannelMetadata METADATA = new ChannelMetadata(false);

    private final ChannelConfig conf = new DefaultChannelConfig(this);

    private SocketAddress localAddress;
    private SocketAddress remoteAddress;
    private volatile boolean open = true;

    public FakeChannel(SocketAddress localAddress, SocketAddress remoteAddress) {
        super(null);
        this.localAddress = localAddress;
        this.remoteAddress = remoteAddress;
    }

    @Override
    public ChannelFuture close() {
        if (SpaghetUtil.whoTouchedSpaghet(1, 2, false) != null) return newFailedFuture(new IllegalCallerException("SOMEBODY TOUCHA MY SPAGHET!"));
        return realClose();
    }

    @Override
    public ChannelFuture close(ChannelPromise promise) {
        if (SpaghetUtil.whoTouchedSpaghet(1, 2, false) != null) return newFailedFuture(new IllegalCallerException("SOMEBODY TOUCHA MY SPAGHET!"));
        return realClose(promise);
    }

    @Override
    protected AbstractUnsafe newUnsafe() {
        return new FakeUnsafe();
    }

    @Override
    protected boolean isCompatible(EventLoop loop) {
        return true;
    }

    @Override
    protected SocketAddress localAddress0() {
        return localAddress;
    }

    @Override
    protected SocketAddress remoteAddress0() {
        return remoteAddress;
    }

    @Override
    protected void doBind(SocketAddress localAddress) {
        this.localAddress = localAddress;
    }

    @Override
    protected void doDisconnect() {
        doClose();
    }

    @Override
    protected void doClose() {
        open = false;
    }

    @Override
    protected void doBeginRead() {}

    @Override
    protected void doWrite(ChannelOutboundBuffer buf) {
        while (buf.current() != null) buf.remove();
    }

    public ChannelFuture realClose() {
        return super.close();
    }

    public ChannelFuture realClose(ChannelPromise promise) {
        return super.close(promise);
    }

    @Override
    public ChannelConfig config() {
        return conf;
    }

    @Override
    public boolean isOpen() {
        return open;
    }

    @Override
    public boolean isActive() {
        return isOpen();
    }

    @Override
    public ChannelMetadata metadata() {
        return METADATA;
    }

    private class FakeUnsafe extends AbstractUnsafe {

        @Override
        public void connect(SocketAddress remoteAddress, SocketAddress localAddress, ChannelPromise promise) {
            promise.setFailure(new UnsupportedOperationException("Cannot connect a fake channel"));
        }

    }

}
