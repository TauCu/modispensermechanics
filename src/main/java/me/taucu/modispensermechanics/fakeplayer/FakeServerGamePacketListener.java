package me.taucu.modispensermechanics.fakeplayer;

import net.minecraft.network.Connection;
import net.minecraft.network.DisconnectionDetails;
import net.minecraft.network.PacketSendListener;
import net.minecraft.network.chat.ChatType;
import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.PlayerChatMessage;
import net.minecraft.network.protocol.Packet;
import net.minecraft.network.protocol.common.*;
import net.minecraft.network.protocol.cookie.ServerboundCookieResponsePacket;
import net.minecraft.network.protocol.game.*;
import net.minecraft.network.protocol.ping.ServerboundPingRequestPacket;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.network.CommonListenerCookie;
import net.minecraft.server.network.ServerGamePacketListenerImpl;
import org.bukkit.event.player.PlayerKickEvent;

@SuppressWarnings("deprecation")
public class FakeServerGamePacketListener extends ServerGamePacketListenerImpl {
    public FakeServerGamePacketListener(MinecraftServer server, Connection connection, FakePlayer player, CommonListenerCookie cookie) {
        super(server, connection, player, cookie);
        // prevent anything from "kicking" us
        this.processedDisconnect = true;
    }

    @Override public void tick() {}
    @Override public void resetPosition() {}
    @Override public void disconnect(Component message) {}
    @Override public void disconnect(Component reason, PlayerKickEvent.Cause cause) {}
    @Override public void addPendingMessage(PlayerChatMessage message) {}
    @Override public void onDisconnect(DisconnectionDetails info) {}
    @Override public void send(Packet<?> packet) {}
    @Override public void send(Packet<?> packet, PacketSendListener sendListener) {}
    @Override public void sendDisguisedChatMessage(Component message, ChatType.Bound params) {}
    @Override public void sendPlayerChatMessage(PlayerChatMessage message, ChatType.Bound params) {}
    @Override public void handlePlayerInput(ServerboundPlayerInputPacket packet) {}
    @Override public void handleMoveVehicle(ServerboundMoveVehiclePacket packet) {}
    @Override public void handleAcceptTeleportPacket(ServerboundAcceptTeleportationPacket packet) {}
    @Override public void handleRecipeBookSeenRecipePacket(ServerboundRecipeBookSeenRecipePacket packet) {}
    @Override public void handleRecipeBookChangeSettingsPacket(ServerboundRecipeBookChangeSettingsPacket packet) {}
    @Override public void handleSeenAdvancements(ServerboundSeenAdvancementsPacket packet) {}
    @Override public void handleCustomCommandSuggestions(ServerboundCommandSuggestionPacket packet) {}
    @Override public void handleSetCommandBlock(ServerboundSetCommandBlockPacket packet) {}
    @Override public void handleSetCommandMinecart(ServerboundSetCommandMinecartPacket packet) {}
    @Override public void handlePickItemFromBlock(ServerboundPickItemFromBlockPacket packet) {}
    @Override public void handlePickItemFromEntity(ServerboundPickItemFromEntityPacket packet) {}
    @Override public void handleRenameItem(ServerboundRenameItemPacket packet) {}
    @Override public void handleSetBeaconPacket(ServerboundSetBeaconPacket packet) {}
    @Override public void handleSetStructureBlock(ServerboundSetStructureBlockPacket packet) {}
    @Override public void handleSetJigsawBlock(ServerboundSetJigsawBlockPacket packet) {}
    @Override public void handleJigsawGenerate(ServerboundJigsawGeneratePacket packet) {}
    @Override public void handleSelectTrade(ServerboundSelectTradePacket packet) {}
    @Override public void handleEditBook(ServerboundEditBookPacket packet) {}
    @Override public void handleEntityTagQuery(ServerboundEntityTagQueryPacket packet) {}
    @Override public void handleMovePlayer(ServerboundMovePlayerPacket packet) {}
    @Override public void handlePlayerAction(ServerboundPlayerActionPacket packet) {}
    @Override public void handleUseItemOn(ServerboundUseItemOnPacket packet) {}
    @Override public void handleUseItem(ServerboundUseItemPacket packet) {}
    @Override public void handleTeleportToEntityPacket(ServerboundTeleportToEntityPacket packet) {}
    @Override public void handleResourcePackResponse(ServerboundResourcePackPacket packet) {}
    @Override public void handlePaddleBoat(ServerboundPaddleBoatPacket packet) {}
    @Override public void handleSetCarriedItem(ServerboundSetCarriedItemPacket packet) {}
    @Override public void handleChat(ServerboundChatPacket packet) {}
    @Override public void handleAnimate(ServerboundSwingPacket packet) {}
    @Override public void handlePlayerCommand(ServerboundPlayerCommandPacket packet) {}
    @Override public void handleCommand(String s) {}
    @Override public void handleClientCommand(ServerboundClientCommandPacket packet) {}
    @Override public void handleInteract(ServerboundInteractPacket packet) {}
    @Override public void handleContainerClose(ServerboundContainerClosePacket packet) {}
    @Override public void handleContainerClick(ServerboundContainerClickPacket packet) {}
    @Override public void handlePlaceRecipe(ServerboundPlaceRecipePacket packet) {}
    @Override public void handleContainerButtonClick(ServerboundContainerButtonClickPacket packet) {}
    @Override public void handleSetCreativeModeSlot(ServerboundSetCreativeModeSlotPacket packet) {}
    @Override public void handleSignUpdate(ServerboundSignUpdatePacket packet) {}
    @Override public void handleKeepAlive(ServerboundKeepAlivePacket packet) {}
    @Override public void handlePlayerAbilities(ServerboundPlayerAbilitiesPacket packet) {}
    @Override public void handleClientInformation(ServerboundClientInformationPacket packet) {}
    @Override public void handleCustomPayload(ServerboundCustomPayloadPacket packet) {}
    @Override public void handleChangeDifficulty(ServerboundChangeDifficultyPacket packet) {}
    @Override public void handleLockDifficulty(ServerboundLockDifficultyPacket packet) {}
    @Override public void handleChatAck(ServerboundChatAckPacket packet) {}
    @Override public void handleChatCommand(ServerboundChatCommandPacket packet) {}
    @Override public void handleSignedChatCommand(ServerboundChatCommandSignedPacket packet) {}
    @Override public void handleChatSessionUpdate(ServerboundChatSessionUpdatePacket packet) {}
    @Override public void handleChunkBatchReceived(ServerboundChunkBatchReceivedPacket packet) {}
    @Override public void handleDebugSampleSubscription(ServerboundDebugSampleSubscriptionPacket packet) {}
    @Override public void handleConfigurationAcknowledged(ServerboundConfigurationAcknowledgedPacket packet) {}
    @Override public void handlePong(ServerboundPongPacket packet) {}
    @Override public void handleContainerSlotStateChanged(ServerboundContainerSlotStateChangedPacket packet) {}
    @Override public void handleBlockEntityTagQuery(ServerboundBlockEntityTagQueryPacket packet) {}
    @Override public void handleCookieResponse(ServerboundCookieResponsePacket packet) {}
    @Override public void handlePingRequest(ServerboundPingRequestPacket packet) {}
    @Override public void handleBundleItemSelectedPacket(ServerboundSelectBundleItemPacket packet) {}
    @Override public void handleClientTickEnd(ServerboundClientTickEndPacket packet) {}
}
