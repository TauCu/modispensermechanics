package me.taucu.modispensermechanics.dispense;

public enum DispenseResult {
    SUCCESS,
    FAILURE,
    NOOP;

    public static DispenseResult of(boolean val) {
        if (val) {
            return SUCCESS;
        } else {
            return FAILURE;
        }
    }
}
