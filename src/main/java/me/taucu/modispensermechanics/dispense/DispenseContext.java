package me.taucu.modispensermechanics.dispense;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.core.dispenser.BlockSource;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.block.DispenserBlock;
import net.minecraft.world.level.block.entity.DispenserBlockEntity;
import net.minecraft.world.level.block.state.BlockState;

import java.util.ArrayList;
import java.util.function.BiConsumer;
import java.util.function.BiFunction;
import java.util.function.Consumer;

import static me.taucu.modispensermechanics.dispense.DispenseResult.NOOP;
import static me.taucu.modispensermechanics.dispense.DispenseResult.SUCCESS;
import static me.taucu.modispensermechanics.dispense.DispenseResult.FAILURE;

public class DispenseContext {

    private final BlockSource src;
    private ItemStack stack;

    private BlockState state;
    private Direction facing;
    private DispenserBlockEntity dispenser;
    private final int slot;

    private ArrayList<BiFunction<DispenseContext, DispenseMechanic, Boolean>> preHandleListeners;
    private ArrayList<BiConsumer<DispenseContext, DispenseMechanic>> postHandleListeners;

    private DispenseResult result = DispenseResult.NOOP;

    private DispenseMechanic defaultHandler;
    private Consumer<DispenseContext> alwaysHandleAnimation;
    private Consumer<DispenseContext> alwaysHandleSound;

    public DispenseContext(BlockSource src, DispenserBlockEntity dispenser, ItemStack stack, int slot) {
        this.src = src;
        this.stack = stack;
        this.dispenser = dispenser;
        this.slot = slot;
    }

    public void update() {
        state = null;
        facing = null;
    }

    public BlockSource source() {
        return src;
    }

    public ServerLevel level() {
        return src.level();
    }

    public BlockPos pos() {
        return src.pos();
    }

    public BlockState state() {
        return state == null ? (state = src.state()) : state;
    }

    public double x() {
        return src.pos().getX() + 0.5D;
    }

    public double y() {
        return src.pos().getY() + 0.5D;
    }

    public double z() {
        return src.pos().getZ() + 0.5D;
    }

    public DispenserBlockEntity dispenser() {
        return dispenser;
    }

    public int slot() {
        return slot;
    }

    public Direction facing() {
        return facing == null ? (facing = state().getValue(DispenserBlock.FACING)) : facing;
    }

    public ItemStack stack() {
        return stack;
    }

    public void stack(ItemStack stack) {
        this.stack = stack;
    }

    public boolean isResult(DispenseResult other) {
        return result == other;
    }

    public boolean isNoOp() {
        return result == NOOP;
    }

    public boolean isSuccess() {
        return result == SUCCESS;
    }

    public boolean isFailure() {
        return result == FAILURE;
    }

    public DispenseResult result() {
        return result;
    }

    public void result(DispenseResult result) {
        this.result = result;
    }

    public void result(boolean val) {
        result(DispenseResult.of(val));
    }

    public void success() {
        result(SUCCESS);
    }

    public void failure() {
        result(FAILURE);
    }

    public void noop() {
        result(NOOP);
    }

    public boolean hasDefaultHandler() {
        return defaultHandler != null;
    }

    public DispenseMechanic defaultHandler() {
        return defaultHandler;
    }

    public void defaultHandler(DispenseMechanic defaultHandler) {
        this.defaultHandler = defaultHandler;
    }

    public Consumer<DispenseContext> alwaysHandleAnimation() {
        return alwaysHandleAnimation;
    }

    public void alwaysHandleAnimation(Consumer<DispenseContext> handler) {
        this.alwaysHandleAnimation = handler;
    }

    public Consumer<DispenseContext> alwaysHandleSound() {
        return alwaysHandleSound;
    }

    public void alwaysHandleSound(Consumer<DispenseContext> handler) {
        this.alwaysHandleSound = handler;
    }

    public void onPreHandle(BiFunction<DispenseContext, DispenseMechanic, Boolean> listener) {
        if (preHandleListeners == null) {
            preHandleListeners = new ArrayList<>(1);
        }
        preHandleListeners.add(listener);
    }

    public boolean firePreHandle(DispenseMechanic recipient) {
        if (preHandleListeners == null) {
            return true;
        } else {
            for (var listener : preHandleListeners) {
                if (!listener.apply(this, recipient)) {
                    return false;
                }
            }
        }
        return true;
    }

    public void onPostHandle(BiConsumer<DispenseContext, DispenseMechanic> listener) {
        if (postHandleListeners == null) {
            postHandleListeners = new ArrayList<>(1);
        }
        postHandleListeners.add(listener);
    }

    public void firePostHandle(DispenseMechanic recipient) {
        if (postHandleListeners != null) {
            for (var listener : postHandleListeners) {
                listener.accept(this, recipient);
            }
        }
    }

}
