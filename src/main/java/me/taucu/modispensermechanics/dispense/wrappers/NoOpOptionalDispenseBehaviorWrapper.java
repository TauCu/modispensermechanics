package me.taucu.modispensermechanics.dispense.wrappers;

import me.taucu.modispensermechanics.dispense.DispenseContext;
import net.minecraft.core.dispenser.OptionalDispenseItemBehavior;
import net.minecraft.world.item.ItemStack;

public class NoOpOptionalDispenseBehaviorWrapper extends OptionalDispenseBehaviorWrapper {

    public NoOpOptionalDispenseBehaviorWrapper(OptionalDispenseItemBehavior behavior) {
        super(behavior);
        setPriority(-50);
    }

    @Override
    public void onDispense(DispenseContext ctx) {
        ItemStack stack = behavior.dispense(ctx.source(), ctx.stack());

        if (getDispenseObject().isSuccess()) {
            ctx.success();
            ctx.stack(stack);
        } else {
            ctx.noop();
        }
    }

}
