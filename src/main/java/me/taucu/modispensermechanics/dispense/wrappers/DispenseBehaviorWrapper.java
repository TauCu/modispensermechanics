package me.taucu.modispensermechanics.dispense.wrappers;

import me.taucu.modispensermechanics.dispense.DispenseContext;
import me.taucu.modispensermechanics.dispense.DispenseMechanic;
import net.minecraft.core.dispenser.DefaultDispenseItemBehavior;
import net.minecraft.core.dispenser.DispenseItemBehavior;

public class DispenseBehaviorWrapper extends DispenseMechanic {

    final DispenseItemBehavior behavior;

    public DispenseBehaviorWrapper(DispenseItemBehavior behavior) {
        this.behavior = behavior;
        setPriority(behavior.getClass() == DefaultDispenseItemBehavior.class ? -1000 : -100);
    }

    @Override
    public void onDispense(DispenseContext ctx) {
        ctx.success();
        ctx.stack(behavior.dispense(ctx.source(), ctx.stack()));
    }

    @Override
    public void playAnimation(DispenseContext ctx) {}

    @Override
    public void playSound(DispenseContext ctx) {}

    public DispenseItemBehavior getBehavior() {
        return behavior;
    }

    @Override
    public Class<?> getDispenseClass() {
        return behavior.getClass();
    }

    @Override
    public Object getDispenseObject() {
        return behavior;
    }

}
