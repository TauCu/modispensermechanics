package me.taucu.modispensermechanics.dispense;

import me.taucu.modispensermechanics.MoDispenserMechanics;
import me.taucu.modispensermechanics.fakeplayer.FakePlayerManager;
import me.taucu.modispensermechanics.listeners.PaperBlockListener;
import me.taucu.modispensermechanics.util.BukkitUtil;
import net.minecraft.core.BlockPos;
import net.minecraft.core.dispenser.BlockSource;
import net.minecraft.core.dispenser.DispenseItemBehavior;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.block.entity.DispenserBlockEntity;

import java.util.AbstractList;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.logging.Level;

public class DispenseItemHandler extends AbstractList<DispenseMechanic> implements DispenseItemBehavior {

    private final Item item;
    private final ArrayList<DispenseMechanic> mechanics = new ArrayList<>();

    private boolean hasInit;

    protected DispenseItemHandler(Item item) {
        this(item, true);
    }

    protected DispenseItemHandler(Item item, boolean autoInitializing) {
        Objects.requireNonNull(item);
        this.item = item;
        this.hasInit = autoInitializing;
    }

    public static DispenseItemHandler create(Item item) {
        return new DispenseItemHandler(item);
    }

    public static DispenseItemHandler createManualInitializing(Item item) {
        return new DispenseItemHandler(item, false);
    }

    @Override
    public ItemStack dispense(BlockSource src, ItemStack stack) {
        if (!mechanics.isEmpty()) {
            // no crashing server please ;)
            try {
                DispenserBlockEntity dispenser = src.blockEntity();

                // find the slot used to dispense
                int slot = findSlot(dispenser, stack);
                if (slot == -1) {
                    MoDispenserMechanics.getInstance().getLogger().severe("Failed to resolve slot for dispense. Please report this.");
                    return stack;
                }

                DispenseContext ctx = new DispenseContext(src, dispenser, stack, slot);
                boolean handled = runMechanics(ctx);
                stack = ctx.stack();

                FakePlayerManager.postHandle();

                if (handled) {
                    return stack;
                }
            } catch (Throwable t) {
                BlockPos pos = src.pos();
                MoDispenserMechanics.getInstance().getLogger().log(Level.SEVERE, "Error while dispensing at position=[%d %d %d] world=%s".formatted(pos.getX(), pos.getY(), pos.getZ(), src.level().serverLevelData.getLevelName()), t);
                return stack;
            }
        }
        // fail dispense if no mechanic handled, or there are no mechanics
        DispenseMechanic.playDispenseFailSound(src.level(), src.pos());
        return stack;
    }

    protected int findSlot(DispenserBlockEntity dispenser, ItemStack stack) {
        int slot = -1;
        if (BukkitUtil.IS_PAPER) {
            slot = PaperBlockListener.currentDispenseSlot;
        } else {
            // we don't have to worry about other plugins screwing with the stack before us if we're on spigot
            List<ItemStack> contents = dispenser.getContents();
            for (int i = 0; i < contents.size(); i++) {
                if (contents.get(i) == stack) {
                    slot = i;
                    break;
                }
            }
        }
        return slot;
    }

    protected boolean runMechanics(DispenseContext ctx) {
        for (DispenseMechanic mechanic : mechanics) {
            if (ctx.firePreHandle(mechanic)) {
                mechanic.onDispense(ctx);
                if (!ctx.isNoOp() || !ctx.stack().is(item)) {
                    handleEffects(mechanic, ctx);
                    ctx.firePostHandle(mechanic);
                    return true;
                } else {
                    ctx.firePostHandle(mechanic);
                }
            }
        }

        if (ctx.hasDefaultHandler()) {
            ctx.defaultHandler().onDefault(ctx);
            handleEffects(ctx.defaultHandler(), ctx);
            return true;
        } else {
            if (ctx.alwaysHandleAnimation() != null) {
                ctx.alwaysHandleAnimation().accept(ctx);
            }
            if (ctx.alwaysHandleSound() != null) {
                ctx.alwaysHandleSound().accept(ctx);
                return true;
            }
        }

        return false;
    }

    protected void handleEffects(DispenseMechanic mechanic, DispenseContext ctx) {
        if (ctx.alwaysHandleAnimation() == null) {
            mechanic.playAnimation(ctx);
        } else {
            ctx.alwaysHandleAnimation().accept(ctx);
        }
        if (ctx.alwaysHandleSound() == null) {
            mechanic.playSound(ctx);
        } else {
            ctx.alwaysHandleSound().accept(ctx);
        }
    }

    public Item getItem() {
        return item;
    }

    public void sort() {
        mechanics.sort((one, two) -> {
            return Double.compare(two.getPriority(), one.getPriority());
        });
    }

    public void init() {
        sort();
        hasInit = true;
        for (DispenseMechanic m : mechanics.toArray(new DispenseMechanic[0])) {
            handleUnpair(m);
            if (!handlePair(m)) {
                handleUnpair(m);
                mechanics.remove(m);
            }
        }
        // something may have added something else
        sort();
        mechanics.trimToSize();
    }

    @Override
    public int size() {
        return mechanics.size();
    }

    @Override
    public DispenseMechanic get(int index) {
        return mechanics.get(index);
    }

    @Override
    public DispenseMechanic set(int index, DispenseMechanic mechanic) {
        if (mechanic.getPairedHandler() != null && mechanic.getPairedHandler() != this) throw new IllegalStateException("Mechanic is already paired");
        DispenseMechanic current = mechanics.set(index, mechanic);
        if (current != null) {
            handleUnpair(current);
        }
        if (hasInit) {
            try {
                mechanic.pair(this);
            } catch (Throwable t) {
                mechanics.set(index, current);
                handlePair(current);
                throw t;
            }
        }
        return current;
    }

    @Override
    public boolean add(DispenseMechanic mechanic) {
        if (mechanic.getPairedHandler() != null && mechanic.getPairedHandler() != this) throw new IllegalStateException("Mechanic is already paired");
        mechanics.add(mechanic);
        if (hasInit) init();
        return true;
    }

    @Override
    public boolean remove(Object o) {
        if (mechanics.remove(o) && o != null) {
            handleUnpair((DispenseMechanic) o);
            return true;
        }
        return false;
    }

    @Override
    public DispenseMechanic remove(int index) {
        DispenseMechanic old = mechanics.remove(index);
        if (old != null) {
            handleUnpair(old);
        }
        return old;
    }

    public DispenseMechanic getByDispenseClass(Class<?> target) {
        for (DispenseMechanic mechanic : mechanics) {
            if (mechanic.getDispenseClass() == target) {
                return mechanic;
            }
        }
        return null;
    }

    public boolean hasDispenseClass(Class<?> target) {
        return getByDispenseClass(target) != null;
    }

    public void disable() {
        for (DispenseMechanic mechanic : this) {
            handleUnpair(mechanic);
        }
        mechanics.clear();
    }

    private boolean handlePair(DispenseMechanic m) {
        try {
            m.pair(this);
            return true;
        } catch (Throwable t) {
            MoDispenserMechanics.getInstance().getLogger().log(Level.SEVERE, "Error while pairing mechanic \"%s\": ".formatted(m.getClass()), t);
        }
        return false;
    }

    private boolean handleUnpair(DispenseMechanic m) {
        try {
            m.unpair(this);
            return true;
        } catch (Throwable t) {
            MoDispenserMechanics.getInstance().getLogger().log(Level.SEVERE, "Error while unpairing mechanic \"%s\": ".formatted(m.getClass()), t);
        }
        return false;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        DispenseItemHandler that = (DispenseItemHandler) o;
        return hasInit == that.hasInit && Objects.equals(item, that.item) && Objects.equals(mechanics, that.mechanics);
    }

    @Override
    public int hashCode() {
        return Objects.hash(item, mechanics, hasInit);
    }

    @Override
    public String toString() {
        return "DispenseItemHandler{" +
                "item=" + item +
                ", mechanics=" + mechanics +
                ", hasInit=" + hasInit +
                ", hashCode=" + Integer.toHexString(hashCode()) +
                '}';
    }

}
