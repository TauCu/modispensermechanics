package me.taucu.modispensermechanics.dispense.mechanics;

import me.taucu.modispensermechanics.dispense.DispenseContext;
import me.taucu.modispensermechanics.dispense.DispenseItemHandler;
import me.taucu.modispensermechanics.dispense.DispenseMechanic;
import me.taucu.modispensermechanics.fakeplayer.FakePlayer;
import me.taucu.modispensermechanics.fakeplayer.FakePlayerManager;
import me.taucu.modispensermechanics.util.DispenserUtil;
import me.taucu.modispensermechanics.util.EntityUtil;
import me.taucu.modispensermechanics.util.ItemStackUtils;
import me.taucu.modispensermechanics.util.config.ToolDurabilityOption;
import net.minecraft.core.BlockPos;
import net.minecraft.core.dispenser.DefaultDispenseItemBehavior;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.sounds.SoundEvents;
import net.minecraft.sounds.SoundSource;
import net.minecraft.util.Mth;
import net.minecraft.world.damagesource.DamageSource;
import net.minecraft.world.entity.*;
import net.minecraft.world.entity.ai.attributes.Attributes;
import net.minecraft.world.entity.decoration.ArmorStand;
import net.minecraft.world.entity.monster.warden.Warden;
import net.minecraft.world.item.SwordItem;
import net.minecraft.world.item.enchantment.EnchantmentHelper;
import net.minecraft.world.level.GameType;
import net.minecraft.world.phys.AABB;
import org.bukkit.Bukkit;
import org.bukkit.NamespacedKey;
import org.bukkit.craftbukkit.block.CraftBlock;
import org.bukkit.event.entity.EntityDamageByBlockEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.persistence.PersistentDataType;

import java.util.HashSet;
import java.util.List;

public class WeaponAttackMechanic extends DispenseMechanic {

    public static final NamespacedKey LAST_ITEM_USE_GAME_TIME_KEY = new NamespacedKey("modispensermechanics", "lastattack");

    public static final HashSet<EntityType<?>> DISABLED_ENTITIES = new HashSet<>();

    public static boolean doSweeping;
    public static boolean respectAttackSpeed = true;

    static double metricsDamageDealt = 0;

    @Override
    public void onDispense(DispenseContext ctx) {
        if (ToolDurabilityOption.use(this, ctx, 1)) {
            List<Entity> victims = ctx.level().getEntities((Entity) null,
                    new AABB(ctx.pos().relative(ctx.facing())).inflate(-0.01),
                    test -> test.isAttackable() && EntitySelector.NO_SPECTATORS.test(test) && !DISABLED_ENTITIES.contains(test.getType()));

            if (callDispenseEvent(ctx)) {
                if (!victims.isEmpty()) {
                    BlockPos pos = ctx.pos();
                    double px = pos.getX() + 0.5, py = pos.getY() + 0.5, pz = pos.getZ() + 0.5;

                    // modeled off of Player#attack
                    float baseAttackDamage = (float) ItemStackUtils.calculateAttribute(ctx.stack(), EquipmentSlot.MAINHAND, Attributes.ATTACK_DAMAGE, 1.0D);

                    float attackDamageSpeedScalar;
                    if (respectAttackSpeed) {
                        long lastUsedTime = DispenserUtil.getPDC(ctx.dispenser())
                                .getOrDefault(LAST_ITEM_USE_GAME_TIME_KEY, PersistentDataType.LONG, (long) -Integer.MAX_VALUE);
                        attackDamageSpeedScalar = ItemStackUtils.getAttackDamageScale(ctx.level().getGameTime(), lastUsedTime, ctx.stack(), 0.5F);
                    } else {
                        attackDamageSpeedScalar = 1.0F;
                    }

                    // requires a fake player because Enchantments like Looting only work for Players for some reason(tm)
                    FakePlayer player = FakePlayerManager.getFakePlayer(ctx.level(), px, py-1.5, pz);
                    player.setGameType(GameType.SURVIVAL);
                    player.getInventory().items.set(0, ctx.stack());
                    player.getInventory().selected = 0;

                    boolean hurt = false;

                    if (victims.size() > 1) {
                        victims.sort(EntityUtil.nearestComparator(px, py, pz));
                    }

                    for (Entity victim : victims) {
                        LivingEntity lvic = victim instanceof LivingEntity living ? living : null;
                        // do not attack entities that shouldn't be attacked or dying living entities
                        if (lvic == null || !lvic.isDeadOrDying()) {
                            float lastHealth = 0;

                            if (lvic != null) {
                                lastHealth = lvic.getHealth();
                            }

                            // bug with wardens constantly angry
                            DamageSource damageSource = player.damageSources().playerAttack(victim instanceof Warden ? null : player);
                            float attackDamage = baseAttackDamage;

                            // modeled off of Player#getEnchantedDamage
                            float enchantmentDamage = EnchantmentHelper.modifyDamage(ctx.level(), ctx.stack(), victim, damageSource, attackDamage) - attackDamage;

                            // modeled off of Player#attack
                            attackDamage *= 0.2F + attackDamageSpeedScalar * attackDamageSpeedScalar * 0.8F;
                            enchantmentDamage *= attackDamageSpeedScalar;

                            // modeled off of Player#attack
                            attackDamage += ctx.stack().getItem().getAttackDamageBonus(victim, attackDamage, damageSource);
                            float totalAttackDamage = attackDamage + enchantmentDamage;

                            if (totalAttackDamage <= 0F)
                                continue;

                            if (fireDispenseEvents) {
                                // paper deprecated entity damage event constructors for removal, while no alternative exists in spigot.
                                @SuppressWarnings("removal")
                                EntityDamageByBlockEvent edbbe = new EntityDamageByBlockEvent(CraftBlock.at(ctx.level(), ctx.pos()), victim.getBukkitEntity(), EntityDamageEvent.DamageCause.ENTITY_ATTACK, totalAttackDamage);
                                Bukkit.getPluginManager().callEvent(edbbe);
                                if (edbbe.isCancelled()) {
                                    break;
                                }
                                totalAttackDamage = (float) edbbe.getDamage();
                            }

                            if (victim.hurtServer(ctx.level(), damageSource, totalAttackDamage)) {
                                hurt = true;

                                // modeled off of Player#attack
                                // applies things such as fire aspect and looting after 1.21
                                EnchantmentHelper.doPostAttackEffectsWithItemSource(ctx.level(), victim, damageSource, ctx.stack());

                                // modeled off of Player#attack
                                float knockback = (float) ItemStackUtils.calculateAttribute(ctx.stack(), EquipmentSlot.MAINHAND, Attributes.ATTACK_KNOCKBACK, 0.0D);
                                knockback = EnchantmentHelper.modifyKnockback(ctx.level(), ctx.stack(), victim, damageSource, knockback);

                                // somewhat modeled off of Player#attack
                                if (knockback > 0) {
                                    if (lvic == null) {
                                        victim.push(-Mth.sin(player.getYRot() * 0.017453292F) * knockback * 0.5F, 0.1D, Mth.cos(player.getYRot() * 0.017453292F) * knockback * 0.5F);
                                    } else {
                                        lvic.knockback(knockback * 0.5F, px - victim.getX(), pz - victim.getZ());
                                    }
                                }

                                // apply sweeping attack
                                if (doSweeping && attackDamageSpeedScalar > 0.9F && ctx.stack().getItem() instanceof SwordItem) {
                                    // modeled off of Player#attack
                                    List<LivingEntity> entitiesToSweep = ctx.level().getEntitiesOfClass(LivingEntity.class, victim.getBoundingBox().inflate(1.0D, 0.25D, 1.0D));
                                    if (!entitiesToSweep.isEmpty()) {
                                        float sweepingDamage = 1.0F + (float) ItemStackUtils.calculateAttributeWithEnchants(ctx.stack(), EquipmentSlot.MAINHAND, Attributes.SWEEPING_DAMAGE_RATIO, 0.0D) * attackDamage;
                                        for (LivingEntity sweptEnt : entitiesToSweep) {
                                            if (sweptEnt != victim && sweptEnt != player
                                                    && (!(sweptEnt instanceof ArmorStand armorStand) || !armorStand.isMarker())
                                                    && player.distanceToSqr(sweptEnt) < 9.0) {
                                                lastHealth = sweptEnt.getHealth();

                                                float totalSweepingDamage = EnchantmentHelper.modifyDamage(ctx.level(), ctx.stack(), victim, damageSource, sweepingDamage) * attackDamageSpeedScalar;
                                                if (fireDispenseEvents) {
                                                    // paper deprecated entity damage event constructors for removal, while no alternative exists in spigot.
                                                    @SuppressWarnings("removal")
                                                    EntityDamageByBlockEvent edbbe = new EntityDamageByBlockEvent(CraftBlock.at(ctx.level(), ctx.pos()), sweptEnt.getBukkitEntity(), EntityDamageEvent.DamageCause.ENTITY_SWEEP_ATTACK, totalSweepingDamage);
                                                    Bukkit.getPluginManager().callEvent(edbbe);
                                                    if (edbbe.isCancelled()) {
                                                        continue;
                                                    }
                                                    totalSweepingDamage = (float) Math.min(Float.MAX_VALUE, edbbe.getDamage());
                                                }

                                                DamageSource sweepingSource = player.damageSources().playerAttack(player);
                                                if (sweptEnt.hurtServer(ctx.level(), sweepingSource, totalSweepingDamage)) {
                                                    sweptEnt.knockback(0.4000000059604645D, px - sweptEnt.getX(), pz - sweptEnt.getZ());
                                                    EnchantmentHelper.doPostAttackEffects(ctx.level(), sweptEnt, sweepingSource);

                                                    metricsDamageDealt += Math.max(0, lastHealth - sweptEnt.getHealth());
                                                }

                                            }
                                        }
                                    }

                                }

                                metricsDamageDealt += lvic == null ? 1 : Math.max(0, lastHealth - lvic.getHealth());
                                break;
                            }

                        }
                    }

                    // reset player inventory
                    player.postDispense();

                    // if we hurt something
                    if (hurt) {
                        // damage weapon if required
                        if (ToolDurabilityOption.get() != ToolDurabilityOption.UNBREAKABLE && ItemStackUtils.hurtAndBreak(1, ctx.stack(), ctx.level())) {
                            ctx.level().playSound(null, pos, SoundEvents.ITEM_BREAK, SoundSource.BLOCKS, 1, 1);
                        }
                        ctx.success();
                        // play strong attack sound if attackDamageSpeedScalar > 0.9F
                        ctx.alwaysHandleSound(ctx2 -> playDispenseAttackSound(ctx2.level(), ctx2.pos(), attackDamageSpeedScalar > 0.9F));
                        return;
                    } else {
                        ctx.alwaysHandleSound(ctx2 -> playDispenseAttackSound(ctx2.level(), ctx2.pos(), false));
                    }
                }

                // set cooldown for next attack
                if (respectAttackSpeed) {
                    DispenserUtil.getPDC(ctx.dispenser())
                            .set(LAST_ITEM_USE_GAME_TIME_KEY, PersistentDataType.LONG, ctx.level().getGameTime());
                }
            }
            // always show particles to warn people that this dispenser means business
            ctx.alwaysHandleAnimation(this::playAnimation);
        }
    }

    @Override
    public void onPair(DispenseItemHandler handler) {
        handler.removeIf(m -> m.getDispenseClass() == DefaultDispenseItemBehavior.class);
    }

    @Override
    public void playAnimation(DispenseContext ctx) {
        // always show particles to warn people that this dispenser means business
        playDispenseAnimation(ctx);
    }

    @Override
    public void playSound(DispenseContext ctx) {
        if (ctx.isSuccess()) {
            playDispenseAttackSound(ctx.level(), ctx.pos(), true);
            playDispenseSuccessSound(ctx.level(), ctx.pos());
        } else if (ctx.isFailure()) {
            playDispenseFailSound(ctx.level(), ctx.pos());
        }
    }

    public static void playDispenseAttackSound(ServerLevel level, BlockPos pos, boolean strongAttack) {
        level.playSeededSound(
                null,
                pos.getX(), pos.getY(), pos.getZ(),
                strongAttack ? SoundEvents.PLAYER_ATTACK_STRONG : SoundEvents.PLAYER_ATTACK_NODAMAGE,
                SoundSource.BLOCKS,
                1.0F, 1.0F,
                level.getRandom().nextLong()
        );
    }

    public static int metricsDamageDealt() {
        int i = Math.max(0, (int) Math.min(Integer.MAX_VALUE, metricsDamageDealt));
        metricsDamageDealt = 0;
        return i;
    }

}
