package me.taucu.modispensermechanics.dispense.mechanics.animaltemptmechanic;

import me.taucu.modispensermechanics.dispense.DispenseContext;
import me.taucu.modispensermechanics.dispense.DispenseMechanic;
import net.minecraft.core.BlockPos;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.ai.goal.TemptGoal;
import net.minecraft.world.entity.ai.goal.WrappedGoal;
import net.minecraft.world.entity.animal.Animal;
import net.minecraft.world.level.ClipContext;
import net.minecraft.world.level.entity.EntityTypeTest;
import net.minecraft.world.phys.AABB;
import net.minecraft.world.phys.shapes.CollisionContext;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class AnimalTemptMechanic extends DispenseMechanic {

    public static final Set<EntityType<?>> DISABLED_ENTITIES = new HashSet<>();

    public static boolean requireLineOfSight = true;
    public static boolean temptBabies = true;
    public static double horizontalDetectionDistance = 8, verticalDetectionDistance = 5;
    public static int temptDuration = 200;
    public static int dispenserTimeout = 40;
    public static int atDispenserTimeout = 80;
    public static int maxCooldown = 400;
    public static int minCooldown = 300;

    public AnimalTemptMechanic() {
        // we always no-op
        setPriority(10);
    }

    @Override
    public void onDispense(DispenseContext ctx) {
        BlockPos facingPos = ctx.pos().relative(ctx.facing());
        if (ctx.level().getBlockState(facingPos).isSolidRender())
            return;

        AABB detectionBox = new AABB(ctx.pos()).inflate(horizontalDetectionDistance, verticalDetectionDistance, horizontalDetectionDistance);
        List<Animal> animals = ctx.level().getEntitiesOfClass(Animal.class, detectionBox, animal -> {
            return animal.isFood(ctx.stack())
                    && !DISABLED_ENTITIES.contains(animal.getType())
                    && (animal.isBaby() ? temptBabies : animal.canFallInLove() || animal.getHealth() < animal.getMaxHealth());
        });

        for (Animal animal : animals) {
            DispenserTemptGoal dispenserTemptGoal = null;
            boolean hasTemptGoal = false;
            int temptGoalPriority = 0;
            for (WrappedGoal availableGoal : animal.goalSelector.getAvailableGoals()) {
                if (availableGoal.getGoal() instanceof DispenserTemptGoal goal) {
                    dispenserTemptGoal = goal;
                    if (hasTemptGoal)
                        break;
                } else if (availableGoal.getGoal() instanceof TemptGoal goal) {
                    hasTemptGoal = true;
                    temptGoalPriority = availableGoal.getPriority();
                    if (dispenserTemptGoal != null)
                        break;
                }
            }

            if (!hasTemptGoal)
                continue;

            if (dispenserTemptGoal == null) {
                dispenserTemptGoal = new DispenserTemptGoal(animal, 1.0D);
                animal.goalSelector.addGoal(temptGoalPriority, dispenserTemptGoal);
            }

            // only require line of sight when activating the goal
            // if it's already running, update it regardless
            if (requireLineOfSight && !dispenserTemptGoal.isRunning()) {
                var clip = new ClipContext(animal.getEyePosition(),
                        ctx.pos().getCenter().relative(ctx.facing(), 0.6),
                        ClipContext.Block.VISUAL,
                        ClipContext.Fluid.NONE,
                        CollisionContext.empty());
                var result = ctx.level().clip(clip);
                if (!facingPos.equals(result.getBlockPos()))
                    continue;
            }
            dispenserTemptGoal.setTargetDispenser(ctx.pos());
        }
    }

    public static void removeTemptGoals() {
        for (ServerLevel level : MinecraftServer.getServer().getAllLevels()) {
            level.getEntities(EntityTypeTest.forClass(Animal.class), animal -> {
                animal.goalSelector.removeAllGoals(goal -> goal instanceof DispenserTemptGoal);
                return false;
            });
        }
    }

}
