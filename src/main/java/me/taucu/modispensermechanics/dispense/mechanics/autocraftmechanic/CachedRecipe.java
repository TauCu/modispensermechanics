package me.taucu.modispensermechanics.dispense.mechanics.autocraftmechanic;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.crafting.*;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

public record CachedRecipe(CraftingRecipe recipe) {

    public static final RecipeManager RECIPE_MANAGER = MinecraftServer.getServer().getRecipeManager();
    public static final Cache<Key, CachedRecipe> RECIPE_CACHE = CacheBuilder.newBuilder()
            .expireAfterWrite(600, TimeUnit.SECONDS)
            .maximumSize(32000)
            .build();

    public static final CachedRecipe INVALID_RECIPE = new CachedRecipe(null);

    public static CachedRecipe getRecipeFor(CraftingInput input, ServerLevel level) {
        Key cacheKey = new Key(input.items());
        CachedRecipe cached = RECIPE_CACHE.getIfPresent(cacheKey);
        if (cached == null) {
            Optional<RecipeHolder<CraftingRecipe>> opt = RECIPE_MANAGER.getRecipeFor(RecipeType.CRAFTING, input, level);
            cacheKey.copyStacksForCaching();
            if (opt.isEmpty()) {
                RECIPE_CACHE.put(cacheKey, INVALID_RECIPE);
                return null;
            } else {
                cached = new CachedRecipe(opt.get().value());
                RECIPE_CACHE.put(cacheKey, cached);
            }
        } else if (cached == INVALID_RECIPE) {
            return null;
        }
        return cached;
    }

    public static class Key {
        private final ItemStack[] stacks;
        private Item[] items;
        private int hashCode;

        public Key(List<ItemStack> in) {
            ItemStack[] stacks = new ItemStack[in.size()];
            Item[] items = new Item[in.size()];
            for (int i = 0; i < stacks.length; i++) {
                ItemStack stack = in.get(i);
                stacks[i] = stack;
                items[i] = stack.getItem();
            }
            this.stacks = stacks;
            this.items = items;
            hashCode = Arrays.hashCode(items);
        }

        public void copyStacksForCaching() {
            for (int i = 0; i < stacks.length; i++) {
                ItemStack stack = stacks[i];
                stacks[i] = stack.copy();
                items[i] = stack.getItem();
            }
            hashCode = Arrays.hashCode(items);
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Key that = (Key) o;
            if (stacks.length != that.stacks.length || !Arrays.equals(items, that.items)) return false;
            for (int i = 0; i < stacks.length; i++) {
                if (!ItemStack.isSameItemSameComponents(stacks[i], that.stacks[i])) return false;
            }
            return true;
        }

        @Override
        public int hashCode() {
            return hashCode;
        }

    }

}
