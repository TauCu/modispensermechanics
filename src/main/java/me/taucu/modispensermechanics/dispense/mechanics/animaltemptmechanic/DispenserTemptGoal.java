package me.taucu.modispensermechanics.dispense.mechanics.animaltemptmechanic;

import net.minecraft.core.BlockPos;
import net.minecraft.util.Mth;
import net.minecraft.world.entity.ai.goal.Goal;
import net.minecraft.world.entity.animal.Animal;
import net.minecraft.world.level.block.DispenserBlock;
import net.minecraft.world.level.block.entity.DispenserBlockEntity;
import net.minecraft.world.phys.Vec3;

import java.util.EnumSet;

public class DispenserTemptGoal extends Goal {

    private final Animal mob;
    private final double speed;
    private final int maxActiveTicks = reducedTickDelay(AnimalTemptMechanic.temptDuration);
    private final int maxTimeout = reducedTickDelay(AnimalTemptMechanic.dispenserTimeout);
    private final int maxTicksAtDispenser = reducedTickDelay(AnimalTemptMechanic.atDispenserTimeout);
    private final int maxDistance;

    private BlockPos dispenserPos;
    private DispenserBlockEntity dispenser;
    private Vec3 lookPos;
    private BlockPos navPos;
    private int ticksActive;
    private int timeout;
    private int ticksAtTarget;
    private int cooldown;
    private boolean isRunning;

    public DispenserTemptGoal(Animal mob, double speed) {
        this.mob = mob;
        this.speed = speed;
        this.maxDistance = Mth.ceil(Mth.square(Math.max(AnimalTemptMechanic.horizontalDetectionDistance, AnimalTemptMechanic.verticalDetectionDistance) + 1 + (mob.getBoundingBox().getXsize() / 2)) * 1.5);
        setFlags(EnumSet.of(Goal.Flag.MOVE, Goal.Flag.LOOK));
    }

    public boolean setTargetDispenser(BlockPos dispenserPos) {
        if (cooldown > 0)
            return false;
        if (isRunning) {
            if (this.dispenserPos.equals(dispenserPos)) {
                this.timeout = 0;
                return true;
            }
            return false;
        }
        this.dispenserPos = dispenserPos;
        return true;
    }

    @Override
    public boolean canUse() {
        if (cooldown > 0) {
            cooldown--;
            return false;
        } else if (dispenserPos != null) {
            if (dispenser == null && getServerLevel(mob).getBlockEntity(dispenserPos) instanceof DispenserBlockEntity dispenserEntity) {
                this.dispenser = dispenserEntity;
            }
            return dispenser != null && !dispenser.isRemoved();
        }
        return false;
    }

    @Override
    public boolean canContinueToUse() {
        if (timeout > maxTimeout || ticksActive > maxActiveTicks)
            return false;
        double dist = mob.getBoundingBox().distanceToSqr(lookPos);
        if (dist > maxDistance)
            return false;
        if (dist < 0.1D && ++ticksAtTarget > maxTicksAtDispenser)
            return false;
        return canUse();
    }

    @Override
    public void start() {
        var facing = dispenser.getBlockState().getValue(DispenserBlock.FACING);
        this.lookPos = Vec3.atCenterOf(dispenserPos).relative(facing, 0.5D);
        this.navPos = dispenserPos.relative(facing);
        this.isRunning = true;
    }

    @Override
    public void stop() {
        this.dispenserPos = null;
        this.dispenser = null;
        this.lookPos = null;
        this.ticksActive = 0;
        this.timeout = 0;
        this.ticksAtTarget = 0;
        this.cooldown = reducedTickDelay(mob.random.nextInt(AnimalTemptMechanic.minCooldown, AnimalTemptMechanic.maxCooldown));
        this.mob.getNavigation().stop();
        this.isRunning = false;
    }

    @Override
    public void tick() {
        timeout++;
        ticksActive++;
        mob.getLookControl().setLookAt(lookPos);
        var path = mob.getNavigation().createPath(navPos, 0);
        if (path != null)
            mob.getNavigation().moveTo(path, speed);
    }

    public boolean isRunning() {
        return isRunning;
    }

}
