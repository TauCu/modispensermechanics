package me.taucu.modispensermechanics.dispense.mechanics;

import me.taucu.modispensermechanics.MoDispenserMechanics;
import me.taucu.modispensermechanics.dispense.DispenseContext;
import me.taucu.modispensermechanics.dispense.DispenseItemHandler;
import me.taucu.modispensermechanics.dispense.DispenseMechanic;
import net.minecraft.core.BlockPos;
import net.minecraft.core.dispenser.DefaultDispenseItemBehavior;
import net.minecraft.core.particles.SimpleParticleType;
import net.minecraft.core.registries.BuiltInRegistries;
import net.minecraft.network.protocol.Packet;
import net.minecraft.network.protocol.game.ClientboundLevelParticlesPacket;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.sounds.SoundEvents;
import net.minecraft.sounds.SoundSource;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.animal.Animal;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.phys.AABB;
import net.minecraft.world.phys.Vec3;

import java.util.HashSet;
import java.util.List;

public class AnimalBreedMechanic extends DispenseMechanic {

    public static final HashSet<EntityType<?>> DISABLED_ANIMALS = new HashSet<>();

    protected static long animalsFed = 0;

    public SimpleParticleType happyVillager;

    public AnimalBreedMechanic() {
        // we don't do things often and we NO-OP if we don't.
        setPriority(50);
        var particleOpt = BuiltInRegistries.PARTICLE_TYPE.get(ResourceLocation.tryParse("minecraft:happy_villager"));
        if (particleOpt.isPresent()) {
            this.happyVillager = (SimpleParticleType) particleOpt.get().value();
        } else {
            MoDispenserMechanics.getInstance().getLogger().warning("AnimalBreedMechanic: could not find happy villager particle");
        }
    }

    @Override
    public void onDispense(DispenseContext ctx) {
        List<Animal> animals = ctx.level().getEntitiesOfClass(Animal.class, new AABB(ctx.pos().relative(ctx.facing())).inflate(-0.1, -0.1, -0.1));
        if (!animals.isEmpty()) {
            for (Animal animal : animals) {
                if (animal.isFood(ctx.stack()) && !DISABLED_ANIMALS.contains(animal.getType())) {
                    if (animal.isBaby()) {
                        if (callDispenseEvent(ctx)) {
                            animal.ageUp(Animal.getSpeedUpSecondsWhenFeeding(-animal.getAge()), true);
                            playHappyVillager(ctx.level(), animal.getBoundingBox());
                            ctx.stack().shrink(1);
                            ctx.success();
                            animalsFed++;
                        }
                        break;
                    } else if (animal.canFallInLove() && animal.getAge() == 0) {
                        if (callDispenseEvent(ctx)) {
                            setInLoveFixed(animal, ctx.stack());
                            ctx.stack().shrink(1);
                            ctx.success();
                            animalsFed++;
                        }
                        break;
                    }
                }
            }
        }
    }

    @Override
    public void onPair(DispenseItemHandler handler) {
        handler.removeIf(m -> m.getDispenseClass() == DefaultDispenseItemBehavior.class);
    }

    @Override
    public void playSound(DispenseContext ctx) {
        if (ctx.isSuccess()) {
            playDispenseBreedSound(ctx.level(), ctx.pos());
        } else {
            super.playSound(ctx);
        }
    }

    public void playHappyVillager(ServerLevel level, AABB box) {
        if (happyVillager == null) return;
        Vec3 center = box.getCenter();
        Packet<?> packet = new ClientboundLevelParticlesPacket(
                happyVillager,
                false,
                false,
                center.x(),
                center.y() + (box.getYsize() / 4),
                center.z(),
                (float) box.getXsize() / 2.5F,
                (float) box.getYsize() / 2.5F,
                (float) box.getZsize() / 2.5F,
                0.2F,
                (int) (box.getSize() * 12) + 1);
        level.getServer().getPlayerList().broadcast(null, center.x(), center.y(), center.z(), 16, level.dimension(), packet);
    }

    public static void playDispenseBreedSound(ServerLevel level, BlockPos pos) {
        level.playSeededSound(
                null,
                pos.getX(), pos.getY(), pos.getZ(),
                SoundEvents.BONE_MEAL_USE,
                SoundSource.BLOCKS,
                1.0F, 0.75F,
                level.getRandom().nextLong()
        );
    }

    public static void setInLoveFixed(Animal animal, ItemStack breedItem) {
        animal.setInLoveTime(600);
        animal.loveCause = null;
        animal.breedItem = breedItem;

        animal.level().broadcastEntityEvent(animal, (byte) 18);
    }

    public static int metricsItemsFedToAnimals() {
        int i = Math.max(0, (int) Math.min(Integer.MAX_VALUE, animalsFed));
        animalsFed = 0;
        return i;
    }

}
