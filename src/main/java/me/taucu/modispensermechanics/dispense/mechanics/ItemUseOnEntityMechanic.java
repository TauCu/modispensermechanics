package me.taucu.modispensermechanics.dispense.mechanics;

import me.taucu.modispensermechanics.dispense.DispenseContext;
import me.taucu.modispensermechanics.dispense.DispenseMechanic;
import me.taucu.modispensermechanics.fakeplayer.FakePlayer;
import me.taucu.modispensermechanics.fakeplayer.FakePlayerManager;
import me.taucu.modispensermechanics.util.EntityUtil;
import net.minecraft.core.BlockPos;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.EntitySelector;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.level.GameType;
import net.minecraft.world.phys.AABB;
import net.minecraft.world.phys.Vec3;

import java.util.HashSet;

public class ItemUseOnEntityMechanic extends DispenseMechanic {

    public static final HashSet<EntityType<?>> DISABLED_ENTITIES = new HashSet<>();

    private static long metricsItemsUsed;

    public ItemUseOnEntityMechanic() {
        setPriority(-55);
    }

    @Override
    public void onDispense(DispenseContext ctx) {
        BlockPos facing = ctx.pos().relative(ctx.facing());
        AABB box = new AABB(facing).inflate(-0.01);
        var entities = ctx.level().getEntities((Entity) null, box, test -> EntitySelector.NO_SPECTATORS.test(test)
                && !DISABLED_ENTITIES.contains(test.getType()));

        if (!entities.isEmpty()) {
            if (entities.size() > 1) {
                entities.sort(EntityUtil.nearestComparator(Vec3.atCenterOf(ctx.pos())));
            }

            FakePlayer player = FakePlayerManager.getFakePlayerCenteredInBlock(ctx.level(), ctx.pos());
            player.setGameType(GameType.SURVIVAL);
            player.getInventory().items.set(0, ctx.stack());
            player.getInventory().selected = 0;

            for (Entity entity : entities) {
                var result = entity.interact(player, InteractionHand.MAIN_HAND);
                var itemInHand = player.getInventory().items.getFirst();
                if (result.consumesAction() || !ctx.stack().equals(itemInHand)) {
                    ctx.success();
                    metricsItemsUsed++;
                    break;
                }
            }

            ItemUseMechanic.playerPostUse(ctx, player, facing, null);
            player.postDispense();
        }
    }

    public static int metricsItemsUsedOnEntities() {
        int i = Math.max(0, (int) Math.min(Integer.MAX_VALUE, metricsItemsUsed));
        metricsItemsUsed = 0;
        return i;
    }

}
