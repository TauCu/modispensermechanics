package me.taucu.modispensermechanics.dispense;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.core.dispenser.DefaultDispenseItemBehavior;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.LevelAccessor;
import org.bukkit.Bukkit;
import org.bukkit.craftbukkit.block.CraftBlock;
import org.bukkit.craftbukkit.inventory.CraftItemStack;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.block.BlockDispenseEvent;
import org.bukkit.util.Vector;

public abstract class DispenseMechanic {

    public static final DefaultDispenseItemBehavior DISPENSE_ITEM_BEHAVIOR = new DefaultDispenseItemBehavior();

    public static boolean fireDispenseEvents = true;

    private final boolean pairing;

    private double priority;
    private DispenseItemHandler pairedHandler;

    public DispenseMechanic() {
        this(true);
    }

    public DispenseMechanic(boolean pairing) {
        this.pairing = pairing;
        setPriority(0);
    }

    public abstract void onDispense(DispenseContext ctx);

    public void onDefault(DispenseContext ctx) {}

    public void playSound(DispenseContext ctx) {
        if (ctx.isSuccess()) {
            playDispenseSuccessSound(ctx.level(), ctx.pos());
        } else if (ctx.isFailure()) {
            playDispenseFailSound(ctx.level(), ctx.pos());
        }
    }

    public void playAnimation(DispenseContext ctx) {
        if (ctx.isSuccess()) {
            playDispenseAnimation(ctx);
        }
    }

    public void onPair(DispenseItemHandler handler) {}

    protected final void pair(DispenseItemHandler handler) {
        if (pairing) {
            if (pairedHandler != null) throw new IllegalStateException("Already paired to: " + pairedHandler);
            this.onPair(handler);
            pairedHandler = handler;
        }
    }

    public void onUnpair(DispenseItemHandler handler) {}

    protected final void unpair(DispenseItemHandler from) {
        if (pairing && pairedHandler != null) {
            DispenseItemHandler handler = pairedHandler;
            if (from == null) throw new NullPointerException("from is null");
            if (from != handler) throw new IllegalArgumentException("Mechanic is paired to the \"%s\" but unpair was called for \"%s\"".formatted(handler, from));
            pairedHandler = null;
            this.onUnpair(handler);
        }
    }

    public final DispenseItemHandler getPairedHandler() {
        return pairedHandler;
    }

    public final boolean usesPairing() {
        return pairing;
    }

    /**
     * Sets the priority of this mechanic. Lower priorities are handled last.
     * @param priority the priority to set. can be negative, must be finite.
     * @throws IllegalArgumentException if priority is not finite
     * @apiNote Negative priorities are normally used by vanilla mechanics
     */
    public final void setPriority(double priority) throws IllegalArgumentException {
        if (!Double.isFinite(priority)) throw new IllegalArgumentException("priority must be finite");
        // make priorities more unique so that future addons can differentiate by decimal
        this.priority = priority + 0.00001525855623 + (this.getClass().getSimpleName().hashCode() & 0xFFFF) / 65536D;
    }

    public final double getPriority() {
        return priority;
    }

    public Class<?> getDispenseClass() {
        return getClass();
    }

    public Object getDispenseObject() {
        return this;
    }

    public static void playDispenseFailSound(LevelAccessor level, BlockPos pos) {
        level.levelEvent(1001, pos, 0);
    }

    public static void playDispenseSuccessSound(LevelAccessor level, BlockPos pos) {
        level.levelEvent(1000, pos, 0);
    }

    public static void playDispenseAnimation(DispenseContext ctx) {
        playDispenseAnimation(ctx.level(), ctx.pos(), ctx.facing());
    }

    public static void playDispenseAnimation(LevelAccessor level, BlockPos pos, Direction facing) {
        level.levelEvent(2000, pos, facing.get3DDataValue());
    }

    public static boolean callDispenseEvent(DispenseContext ctx) {
        return callDispenseEvent(ctx.level(), ctx.pos(), ctx.stack());
    }

    public static boolean callDispenseEvent(LevelAccessor level, BlockPos pos, ItemStack stack) {
        if (fireDispenseEvents) {
            BlockDispenseEvent dispenseEvent = new BlockDispenseEvent(
                    CraftBlock.at(level, pos),
                    CraftItemStack.asCraftMirror(stack),
                    new Vector(0, 0, 0)
            );

            return callEvent(dispenseEvent);
        }
        return true;
    }

    public static boolean callEvent(Event e) {
        Bukkit.getPluginManager().callEvent(e);
        return !(e instanceof Cancellable ce) || !ce.isCancelled();
    }

}
