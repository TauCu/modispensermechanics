package me.taucu.modispensermechanics.dispense.mock;

import me.taucu.modispensermechanics.dispense.DispenseContext;
import me.taucu.modispensermechanics.dispense.DispenseMechanic;
import me.taucu.modispensermechanics.util.BukkitUtil;
import net.minecraft.core.component.DataComponents;
import net.minecraft.core.dispenser.DispenseItemBehavior;
import net.minecraft.core.dispenser.EquipmentDispenseItemBehavior;

public class MockEquipmentDispenseBehavior extends DispenseMechanic {

    public MockEquipmentDispenseBehavior() {
        setPriority(-25);
    }

    @Override
    public void onDispense(DispenseContext ctx) {
        if (ctx.stack().has(DataComponents.EQUIPPABLE)) {
            ctx.result(dispenseEquipment(ctx));
        }
    }

    @SuppressWarnings("deprecation")
    public static boolean dispenseEquipment(DispenseContext ctx) {
        if (BukkitUtil.IS_PAPER) {
            return EquipmentDispenseItemBehavior.dispenseEquipment(ctx.source(), ctx.stack(), DispenseItemBehavior.NOOP);
        } else {
            return EquipmentDispenseItemBehavior.dispenseEquipment(ctx.source(), ctx.stack());
        }
    }

}
