package me.taucu.modispensermechanics.scheduler;

public abstract class TickTask {

    TaskTicker ticker;
    int id = -1;

    public abstract void tick();

    public void onStart() {}

    public void onStop() {}

    public void stop() {
        if (ticker != null) {
            ticker.stop(this);
            ticker = null;
        }
    }

}
