package me.taucu.modispensermechanics;

import me.taucu.modispensermechanics.dispense.DispenseMechanic;
import me.taucu.modispensermechanics.fakeplayer.FakePlayer;
import me.taucu.modispensermechanics.fakeplayer.FakePlayerManager;
import me.taucu.modispensermechanics.listeners.ChunkListener;
import me.taucu.modispensermechanics.listeners.EntityListener;
import me.taucu.modispensermechanics.listeners.PaperBlockListener;
import me.taucu.modispensermechanics.listeners.PaperEntityListener;
import me.taucu.modispensermechanics.net.DuplexHandlerImpl;
import me.taucu.modispensermechanics.scheduler.TaskTicker;
import me.taucu.modispensermechanics.util.BukkitUtil;
import me.taucu.modispensermechanics.util.config.ConfigFileUtil;
import me.taucu.modispensermechanics.util.config.ToolDurabilityOption;
import org.bstats.bukkit.Metrics;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.Configuration;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.net.InetAddress;
import java.util.List;
import java.util.logging.Level;

public class MoDispenserMechanics extends JavaPlugin {

    public static final List<String> SERVER_VERSIONS = List.of("1.21.4");

    private static MoDispenserMechanics instance;

    private CustomBehaviorManager manager;
    private TaskTicker ticker = new TaskTicker(this);
    private Metrics metrics;

    public static MoDispenserMechanics getInstance() {
        return instance;
    }

    public CustomBehaviorManager getManager() {
        return manager;
    }

    public TaskTicker getTicker() {
        return ticker;
    }

    // paper bullshitting about setEnabled.
    @SuppressWarnings({"UnstableApiUsage", "deprecation"})
    @Override
    public void onEnable() {
        instance = this;
        if (!SERVER_VERSIONS.contains(BukkitUtil.getMinecraftVersion())) {
            getLogger().warning("This plugin officially supports [" + String.join(", ", SERVER_VERSIONS) + "] but your server is running " + BukkitUtil.getMinecraftVersion());
        }

        if (BukkitUtil.isRunning()) {
            if (BukkitUtil.isReloading()) {
                getLogger().warning("You are using /reload. This may cause issues.");
            } else {
                getLogger().warning("You are reloading this plugin with a plugin manager. This may cause issues.");
            }
        }

        this.reloadConfig();

        ticker.init();

        try {
            FakePlayerManager.preInit();

            this.manager = new CustomBehaviorManager(this);
            manager.init();

            Bukkit.getPluginManager().registerEvents(new ChunkListener(), this);

            EntityListener entityListener = BukkitUtil.IS_PAPER ? new PaperEntityListener() : new EntityListener();
            Bukkit.getPluginManager().registerEvents(entityListener, this);

            if (BukkitUtil.IS_PAPER) Bukkit.getPluginManager().registerEvents(new PaperBlockListener(), this);

            if (BukkitUtil.isRunning()) {
                entityListener.enforceListenerPriorities();
                Bukkit.getOnlinePlayers().forEach(p -> attachHandler(p, p.getAddress().getAddress()));
                FakePlayerManager.init();
                Bukkit.getScheduler().runTask(this, entityListener::enforceListenerPriorities);
                Bukkit.getOnlinePlayers().forEach(p -> p.showPlayer(FakePlayerManager.getFakePlayer().getBukkitEntity()));
            } else {
                Bukkit.getScheduler().runTask(this, () -> {
                    try {
                        entityListener.enforceListenerPriorities();
                        FakePlayerManager.init();
                    } catch (Throwable t) {
                        getLogger().log(Level.SEVERE, "Error during init. Have you updated?", t);
                        setEnabled(false);
                    }
                });
            }
        } catch (Throwable t) {
            getLogger().log(Level.SEVERE, "Error while running pre-init. Have you updated?", t);
            setEnabled(false);
            return;
        }

        try {
            this.metrics = new Metrics(this, 14840);
            manager.createMechanicMetrics(metrics);
        } catch (Throwable t) {
            getLogger().log(Level.SEVERE, "Error while setting up metrics. Plugin will still function but this shouldn't happen.", t);
        }
    }

    @Override
    public void onDisable() {
        if (this.manager != null)
            manager.shutdown();
        ticker.shutdown();
        FakePlayerManager.shutdown();
        Bukkit.getOnlinePlayers().stream()
                .filter(p -> !p.hasMetadata("NPC"))
                .forEach(p -> DuplexHandlerImpl.detach(p, DuplexHandlerImpl.NAME));
    }

    @Override
    public void reloadConfig() {
        if (manager != null)
            manager.shutdown();

        saveDefaultConfig();
        super.reloadConfig();
        Configuration conf = getConfig();
        Configuration defaults = ConfigFileUtil.loadInternalConfig();
        if (ConfigFileUtil.minorMismatch(conf, defaults)) {
            if (ConfigFileUtil.majorMismatch(conf, defaults)) {
                getLogger().warning("outdated configuration major version. regenerating.");
                ConfigFileUtil.renameOldConfig(new File(getDataFolder(), "config.yml"));
                saveDefaultConfig();
                super.reloadConfig();
                conf = getConfig();
            } else {
                getLogger().warning("outdated configuration minor version. it is recommended to regenerate the config.");
            }
        }
        conf.setDefaults(defaults);

        ToolDurabilityOption.set(ToolDurabilityOption.resolve(conf.getString("general.tool durability option")));
        DispenseMechanic.fireDispenseEvents = conf.getBoolean("general.dispense events");
        FakePlayer.allowFakePlayerEvents = conf.getBoolean("general.fake player events");
        EntityListener.allowTaming = conf.getBoolean("general.allow taming");

        if (manager != null)
            manager.init();
    }

    @SuppressWarnings("deprecation")
    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (args.length > 0) {
            switch (args[0].toLowerCase()) {
                case "reload":
                    if (sender.hasPermission("tau.modispensermechanics.reload")) {
                        sender.sendMessage("Reloading...");
                        reloadConfig();
                        sender.sendMessage("Reloaded.");
                    } else {
                        sender.sendMessage(ChatColor.RED + "You do not have permission to reload MoDispenserMechanics.");
                    }
                    break;
                default:
                    sender.sendMessage("Usage: /modispensermechanics <reload>");
                    break;
            }
        } else {
            sender.sendMessage(
                    ChatColor.AQUA + "MoDispenserMechanics" + ChatColor.GRAY + " by " + ChatColor.AQUA + "TauCubed" + ChatColor.GRAY + " version " + ChatColor.AQUA + getDescription().getVersion()
                    + "\n" + ChatColor.GRAY + " \u2514 " + getDescription().getDescription()
                    + "\n" + ChatColor.GRAY + "Usage: /modispensermechanics <reload>"
            );
        }
        return true;
    }

    List<String> baseComplete = List.of("reload");
    @Override
    public List<String> onTabComplete(CommandSender sender, Command command, String alias, String[] args) {
        return baseComplete.stream()
                .filter(s -> args.length == 0 || (args.length == 1 && s.toLowerCase().startsWith(args[0].toLowerCase())))
                .toList();
    }

    public static void attachHandler(Player player, InetAddress address) {
        try {
            if (!player.hasMetadata("NPC")) {
                new DuplexHandlerImpl()
                        .attach(address);
            }
        } catch (Exception ex) {
            MoDispenserMechanics inst = MoDispenserMechanics.getInstance();
            inst.getLogger().warning("Failed to attach network handler to \"" + player.getName() + "\"");
        }
    }

}
