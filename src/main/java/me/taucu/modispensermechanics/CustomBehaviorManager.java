package me.taucu.modispensermechanics;

import me.taucu.modispensermechanics.dispense.DispenseItemHandler;
import me.taucu.modispensermechanics.dispense.DispenseMechanic;
import me.taucu.modispensermechanics.dispense.mechanics.*;
import me.taucu.modispensermechanics.dispense.mechanics.animaltemptmechanic.AnimalTemptMechanic;
import me.taucu.modispensermechanics.dispense.mechanics.autocraftmechanic.AutoCraftMechanic;
import me.taucu.modispensermechanics.dispense.mechanics.autocraftmechanic.CachedRecipe;
import me.taucu.modispensermechanics.dispense.mock.MockEquipmentDispenseBehavior;
import me.taucu.modispensermechanics.dispense.tasks.ToolBreakBlockTask;
import me.taucu.modispensermechanics.dispense.wrappers.DispenseBehaviorWrapper;
import me.taucu.modispensermechanics.dispense.wrappers.NoOpOptionalDispenseBehaviorWrapper;
import me.taucu.modispensermechanics.dispense.wrappers.OptionalDispenseBehaviorWrapper;
import me.taucu.modispensermechanics.event.RegisterMechanicsEvent;
import me.taucu.modispensermechanics.util.config.ConfigUtil;
import net.minecraft.core.Holder;
import net.minecraft.core.HolderSet;
import net.minecraft.core.Registry;
import net.minecraft.core.RegistryAccess;
import net.minecraft.core.component.DataComponents;
import net.minecraft.core.dispenser.DefaultDispenseItemBehavior;
import net.minecraft.core.dispenser.DispenseItemBehavior;
import net.minecraft.core.dispenser.OptionalDispenseItemBehavior;
import net.minecraft.core.dispenser.ShearsDispenseItemBehavior;
import net.minecraft.core.registries.Registries;
import net.minecraft.server.MinecraftServer;
import net.minecraft.tags.ItemTags;
import net.minecraft.world.item.*;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.DispenserBlock;
import org.bstats.bukkit.Metrics;
import org.bstats.charts.SingleLineChart;
import org.bukkit.Bukkit;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;

import java.util.*;
import java.util.logging.Logger;
import java.util.stream.Stream;

public class CustomBehaviorManager {

    final HashMap<Item, DispenseItemHandler> currentHandlers = new HashMap<>();
    final HashMap<Item, DispenseItemBehavior> oldBehaviorsRevert = new HashMap<>();

    final MoDispenserMechanics pl;
    final Logger log;

    public CustomBehaviorManager(MoDispenserMechanics pl) {
        this.pl = pl;
        this.log = pl.getLogger();
    }

    public void init() {
        FileConfiguration conf = pl.getConfig();
        RegistryAccess registries = MinecraftServer.getServer().registryAccess();
        Registry<Item> itemRegistry = registries.lookupOrThrow(Registries.ITEM);
        Registry<Block> blockRegistry = registries.lookupOrThrow(Registries.BLOCK);

        // BlockPlace mechanic
        if (conf.getBoolean("mechanics.block place mechanic.enabled")) {
            log.info("registering block place mechanic");
            HashSet<Item> disabledItems = ConfigUtil.resolve(
                    conf.getStringList("mechanics.block place mechanic.disabled items"),
                    itemRegistry,
                    "block place mechanic.disabled items");

            // register BlockPlaceMechanic for all BlockItems
            itemRegistry.stream()
                    .filter(item -> item instanceof BlockItem && !disabledItems.contains(item))
                    .forEach(item -> registerMechanic(item, new BlockPlaceMechanic()));
        }

        // ToolBreakBlock mechanic
        if (conf.getBoolean("mechanics.tool break block mechanic.enabled")) {
            log.info("registering tool break block mechanic");
            ToolBreakBlockTask.speedMultiplier = (float) conf.getDouble("mechanics.tool break block mechanic.speed multiplier");
            ToolBreakBlockMechanic.disabledBlocks.clear();
            ToolBreakBlockMechanic.disabledTools.clear();

            ToolBreakBlockMechanic.disabledBlocks.addAll(
                    ConfigUtil.resolve(
                            conf.getStringList("mechanics.tool break block mechanic.disabled blocks"),
                            blockRegistry,
                            "tool break block mechanic.disabled blocks"));

            ToolBreakBlockMechanic.disabledTools.addAll(
                    ConfigUtil.resolve(
                            conf.getStringList("mechanics.tool break block mechanic.disabled tools"),
                            itemRegistry, "tool break block mechanic.disabled tools"));

            ToolBreakBlockMechanic.noopOnFailure = conf.getBoolean("mechanics.tool break block mechanic.noop on failure");

            // register ToolBreakBlockMechanic for all Tools
            itemRegistry.stream()
                    .filter(ToolBreakBlockMechanic::isTool)
                    .forEach(item -> registerMechanic(item, new ToolBreakBlockMechanic()));

            ToolBreakBlockMechanic.setEnabled(true);
        } else {
            ToolBreakBlockMechanic.setEnabled(false);
        }

        // ItemUse mechanic
        if (conf.getBoolean("mechanics.item use mechanic.enabled")) {
            log.info("registering item use mechanic");
            ItemUseMechanic.interactFacingBlockFirst = conf.getBoolean("mechanics.item use mechanic.interact facing block first");
            ItemUseMechanic.interactRange = conf.getDouble("mechanics.item use mechanic.interact range");

            ItemUseMechanic.BLOCK_USE_BEHAVIORS.clear();
            ItemUseMechanic.ITEM_USE_BEHAVIORS.clear();

            ConfigUtil.resolve(
                    conf.getStringList("mechanics.item use mechanic.disabled blocks"),
                    blockRegistry,
                    "item use mechanic.disabled blocks"
            ).forEach(b -> ItemUseMechanic.behaviorFor(b, true).disabled = true);

            ConfigUtil.resolve(
                    conf.getStringList("mechanics.item use mechanic.sneak when using on"),
                    blockRegistry,
                    "item use mechanic.sneak when using on"
            ).forEach(b -> ItemUseMechanic.behaviorFor(b, true).sneaking = true);

            ConfigurationSection itemBehaviors = conf.getConfigurationSection("mechanics.item use mechanic.item behaviors");
            for (String k : itemBehaviors.getKeys(false)) {
                for (Item item : ConfigUtil.resolve(List.of(k), itemRegistry, "mechanics.item use mechanic.item behaviors")) {
                    ConfigurationSection behaviorConf = itemBehaviors.getConfigurationSection(k);
                    ItemUseMechanic.ItemUseBehavior behavior = ItemUseMechanic.behaviorFor(item, true);
                    behavior.enabled = item != Items.EGG && behaviorConf.getBoolean("enabled", behavior.enabled);
                    behavior.preventVanilla = behaviorConf.getBoolean("prevent vanilla behavior", behavior.preventVanilla);
                    behavior.blockStateUse = behaviorConf.getBoolean("blockstate use", behavior.blockStateUse);
                    behavior.itemStackUseOn = behaviorConf.getBoolean("itemstack use on", behavior.itemStackUseOn);
                    behavior.itemStackUse = behaviorConf.getBoolean("itemstack use", behavior.itemStackUse && !(item.components().has(DataComponents.EQUIPPABLE)));
                }
            }

            HashSet<Item> preventVanilla = ConfigUtil.resolve(
                    conf.getStringList("mechanics.item use mechanic.prevent vanilla behavior for"),
                    itemRegistry, "mechanics.item use mechanic.prevent vanilla behavior for");

            // register ItemUseMechanic for all ITEMS
            itemRegistry.stream()
                    .filter(item -> ItemUseMechanic.getBehaviorFor(item).enabled)
                    .forEach(item -> registerMechanic(item, new ItemUseMechanic(ItemUseMechanic.getBehaviorFor(item).preventVanilla)));
        }

        if (conf.getBoolean("mechanics.auto craft mechanic.enabled")) {
            log.info("registering auto craft mechanic");
            CachedRecipe.RECIPE_CACHE.invalidateAll();

            itemRegistry.stream()
                    .forEach(item -> registerMechanic(item, new AutoCraftMechanic()));
        }

        // WeaponAttack mechanic
        if (conf.getBoolean("mechanics.weapon attack mechanic.enabled")) {
            log.info("registering weapon attack mechanic");
            WeaponAttackMechanic.doSweeping = conf.getBoolean("mechanics.weapon attack mechanic.do sweeping attack");
            WeaponAttackMechanic.respectAttackSpeed = conf.getBoolean("mechanics.weapon attack mechanic.respect attack speed");

            WeaponAttackMechanic.DISABLED_ENTITIES.clear();
            WeaponAttackMechanic.DISABLED_ENTITIES.addAll(
                    ConfigUtil.resolve(
                            conf.getStringList("mechanics.weapon attack mechanic.disabled entities"),
                            registries.lookupOrThrow(Registries.ENTITY_TYPE),
                            "disabled entities"));

            HashSet<Item> disabledWeapons = ConfigUtil.resolve(
                    conf.getStringList("mechanics.weapon attack mechanic.disabled weapons"),
                    itemRegistry, "weapon attack mechanic.disabled weapons");

            // register WeaponAttackMechanic for all "weapons"
            for (Item item : itemRegistry) {
                if ((item instanceof SwordItem || item instanceof AxeItem) && !disabledWeapons.contains(item)) {
                    registerMechanic(item, new WeaponAttackMechanic());
                }
            }
        }

        List<Item> animalFoodItems = Stream.of(ItemTags.SNIFFER_FOOD, ItemTags.PIGLIN_FOOD, ItemTags.FOX_FOOD, ItemTags.COW_FOOD, ItemTags.GOAT_FOOD,
                        ItemTags.SHEEP_FOOD, ItemTags.WOLF_FOOD, ItemTags.CAT_FOOD, ItemTags.HORSE_FOOD, ItemTags.CAMEL_FOOD, ItemTags.ARMADILLO_FOOD,
                        ItemTags.BEE_FOOD, ItemTags.CHICKEN_FOOD, ItemTags.FROG_FOOD, ItemTags.HOGLIN_FOOD, ItemTags.LLAMA_FOOD, ItemTags.OCELOT_FOOD,
                        ItemTags.PANDA_FOOD, ItemTags.PIG_FOOD, ItemTags.RABBIT_FOOD, ItemTags.STRIDER_FOOD, ItemTags.TURTLE_FOOD, ItemTags.PARROT_FOOD,
                        ItemTags.PARROT_POISONOUS_FOOD, ItemTags.AXOLOTL_FOOD)
                .map(itemRegistry::get)
                .filter(Optional::isPresent)
                .map(Optional::get)
                .flatMap(HolderSet.Named::stream)
                .map(Holder::value)
                .distinct()
                .toList();

        // AnimalBreed mechanic
        if (conf.getBoolean("mechanics.animal breed mechanic.enabled", false)) {
            log.info("registering animal breed mechanic");
            AnimalBreedMechanic.DISABLED_ANIMALS.clear();
            AnimalBreedMechanic.DISABLED_ANIMALS.addAll(
                    ConfigUtil.resolve(
                            conf.getStringList("mechanics.animal breed mechanic.disabled animals"),
                            registries.lookupOrThrow(Registries.ENTITY_TYPE),
                            "animal breed mechanic.disabled animals")
            );

            animalFoodItems.forEach(item -> registerMechanic(item, new AnimalBreedMechanic()));
        }

        // EntityInteract mechanic
        if (conf.getBoolean("mechanics.entity interact mechanic.enabled")) {
            log.info("registering entity interact mechanic");
            ItemUseOnEntityMechanic.DISABLED_ENTITIES.clear();
            ItemUseOnEntityMechanic.DISABLED_ENTITIES.addAll(
                    ConfigUtil.resolve(
                            conf.getStringList("mechanics.entity interact mechanic.disabled entities"),
                            registries.lookupOrThrow(Registries.ENTITY_TYPE),
                            "entity interact mechanic.disabled entities")
            );
            var disabledItems = ConfigUtil.resolve(
                    conf.getStringList("mechanics.entity interact mechanic.disabled items"),
                    itemRegistry,
                    "entity interact mechanic.disabled items");
            for (Item item : itemRegistry) {
                if (!disabledItems.contains(item)) {
                    registerMechanic(item, new ItemUseOnEntityMechanic());
                }
            }
        }

        // AnimalTempt mechanic
        if (conf.getBoolean("mechanics.animal tempt mechanic.enabled")) {
            log.info("registering animal tempt mechanic");
            AnimalTemptMechanic.DISABLED_ENTITIES.clear();
            AnimalTemptMechanic.DISABLED_ENTITIES.addAll(
                    ConfigUtil.resolve(
                            conf.getStringList("mechanics.animal teempt mechanic.disabled entities"),
                            registries.lookupOrThrow(Registries.ENTITY_TYPE),
                            "animal tempt mechanic.disabled entities"
                    )
            );
            AnimalTemptMechanic.requireLineOfSight = conf.getBoolean("mechanics.animal tempt mechanic.require line of sight");
            AnimalTemptMechanic.temptBabies = conf.getBoolean("mechanics.animal tempt mechanic.tempt babies");
            AnimalTemptMechanic.horizontalDetectionDistance = Math.max(1, Math.min(100, conf.getInt("mechanics.animal tempt mechanic.horizontal tempt distance")));
            AnimalTemptMechanic.verticalDetectionDistance = Math.max(1, Math.min(100, conf.getInt("mechanics.animal tempt mechanic.vertical tempt distance")));
            AnimalTemptMechanic.temptDuration = Math.max(0, Math.min(10000, conf.getInt("mechanics.animal tempt mechanic.tempt duration")));
            AnimalTemptMechanic.dispenserTimeout = Math.max(0, conf.getInt("mechanics.animal tempt mechanic.dispenser timeout"));
            AnimalTemptMechanic.atDispenserTimeout = Math.max(0, conf.getInt("mechanics.animal tempt mechanic.at dispenser timeout"));
            AnimalTemptMechanic.minCooldown = Math.max(0, conf.getInt("mechanics.animal tempt mechanic.min cooldown"));
            AnimalTemptMechanic.maxCooldown = Math.max(AnimalTemptMechanic.minCooldown, conf.getInt("mechanics.animal tempt mechanic.max cooldown"));
            animalFoodItems.forEach(item -> registerMechanic(item, new AnimalTemptMechanic()));
        }

        // register vanilla mock behaviors
        itemRegistry.forEach(item -> registerMechanic(item, new MockEquipmentDispenseBehavior()));

        // let other plugins register handlers
        Bukkit.getPluginManager().callEvent(new RegisterMechanicsEvent(this));

        // handlers are on manual init for the first time, we must initialize them
        getCustomBehaviors().forEach((k, v) -> v.init());
    }

    public void shutdown() {
        if (pl.getConfig().getBoolean("mechanics.animal tempt mechanic.enabled"))
            AnimalTemptMechanic.removeTemptGoals();

        currentHandlers.forEach(this::revertBehavior);
        currentHandlers.clear();
        oldBehaviorsRevert.clear();
    }

    public Map<Item, DispenseItemHandler> getCustomBehaviors() {
        return new HashMap<>(currentHandlers);
    }

    public DispenseItemHandler getMechanicsFor(Item item) {
        if (!currentHandlers.containsKey(item)) {
            registerMechanic(item, null);
        }
        return currentHandlers.get(item);
    }

    public DispenseItemHandler registerMechanic(Item item, DispenseMechanic mechanic) {
        DispenseItemBehavior current = DispenserBlock.DISPENSER_REGISTRY.get(item);
        if (current instanceof DispenseItemHandler handler) {
            if (mechanic != null) {
                handler.add(mechanic);
            }
            return handler;
        } else {
            DispenseItemHandler handler = DispenseItemHandler.createManualInitializing(item);
            if (current != null) {
                oldBehaviorsRevert.put(item, current);
                if (current instanceof OptionalDispenseItemBehavior opt) {
                    // special case for handlers that don't drop the item. We can safely chain them
                    if (current instanceof ShearsDispenseItemBehavior) {
                        handler.add(new NoOpOptionalDispenseBehaviorWrapper(opt));
                    } else { // otherwise assume they will drop the item
                        handler.add(new OptionalDispenseBehaviorWrapper(opt));
                    }
                } else {
                    handler.add(new DispenseBehaviorWrapper(current));
                }
            }
            if (mechanic != null) {
                handler.add(mechanic);
            }
            currentHandlers.put(item, handler);
            DispenserBlock.DISPENSER_REGISTRY.put(item, handler);
            return handler;
        }
    }

    public boolean unregisterMechanic(DispenseMechanic mechanic) {
        Item item = null;
        for (var entry : currentHandlers.entrySet()) {
            if (entry.getValue().contains(mechanic)) {
                item = entry.getKey();
                break;
            }
        }
        return item != null && unregisterMechanic(item, mechanic);
    }

    public boolean unregisterMechanic(Item item, DispenseMechanic mechanic) {
        DispenseItemHandler current = currentHandlers.get(item);
        if (current != null && current.remove(mechanic)) {
            if (current.isEmpty()) {
                revertBehavior(item, current);
            }
            return true;
        }
        return false;
    }

    private boolean revertBehavior(Item item, DispenseItemHandler newBehavior) {
        DispenseItemBehavior currentBehavior = DispenserBlock.DISPENSER_REGISTRY.get(item);
        DispenseItemBehavior oldBehavior = oldBehaviorsRevert.get(item);
        // check to see if any other plugins may have altered the behavior since we captured it
        if (currentBehavior == newBehavior || currentBehavior == null || currentBehavior.getClass() == DefaultDispenseItemBehavior.class) {
            newBehavior.disable();
            DispenserBlock.registerBehavior(item, oldBehavior);
            return true;
        }
        return false;
    }

    protected void createMechanicMetrics(Metrics metrics) {
        metrics.addCustomChart(new SingleLineChart("blocks_placed", BlockPlaceMechanic::metricsBlocksPlaced));
        metrics.addCustomChart(new SingleLineChart("blocks_broken", ToolBreakBlockTask::metricsBlocksBroken));
        metrics.addCustomChart(new SingleLineChart("items_used", ItemUseMechanic::metricsItemsUsed));
        metrics.addCustomChart(new SingleLineChart("items_used_on_entities", ItemUseOnEntityMechanic::metricsItemsUsedOnEntities));
        metrics.addCustomChart(new SingleLineChart("damage_dealt", WeaponAttackMechanic::metricsDamageDealt));
        metrics.addCustomChart(new SingleLineChart("items_crafted", AutoCraftMechanic::metricsItemsCrafted));
        metrics.addCustomChart(new SingleLineChart("items_fed_to_animals", AnimalBreedMechanic::metricsItemsFedToAnimals));
    }

}
