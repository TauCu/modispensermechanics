package me.taucu.modispensermechanics.event.internal;

import org.bukkit.entity.Player;
import org.bukkit.event.HandlerList;
import org.bukkit.event.player.PlayerLoginEvent;
import org.jetbrains.annotations.NotNull;

import java.net.InetAddress;

public class FakePlayerLoginEvent extends PlayerLoginEvent {

    public FakePlayerLoginEvent(@NotNull final Player player, @NotNull final String hostname, @NotNull final InetAddress address) {
        super(player, hostname, address);
    }

    @Override
    public void setResult(@NotNull Result result) {}

    @Override
    @SuppressWarnings("deprecation")
    public void setKickMessage(@NotNull String message) {}

    public static HandlerList getHandlerList() {
        return PlayerLoginEvent.getHandlerList();
    }

}
