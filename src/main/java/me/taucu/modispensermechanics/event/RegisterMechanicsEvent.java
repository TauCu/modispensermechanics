package me.taucu.modispensermechanics.event;

import me.taucu.modispensermechanics.CustomBehaviorManager;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.jetbrains.annotations.NotNull;

public class RegisterMechanicsEvent extends Event {

    private static final HandlerList handlers = new HandlerList();

    private final CustomBehaviorManager man;

    public RegisterMechanicsEvent(CustomBehaviorManager man) {
        this.man = man;
    }

    public CustomBehaviorManager getBehaviorManager() {
        return man;
    }

    @Override
    public @NotNull HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

}
